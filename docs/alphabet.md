# Alphabet

Any **Set** of `symbols` or `characters`, indicates with $\Sigma$ with three properties:

1. **Finite**
    - It has finite number of symbols
    - $|\Sigma| = k$
2. **Non Empty**
    - It contains at least one symbol
    - $\Sigma \neq \{\}$ = $\emptyset$
3. **Atomic Symbols**
    - Symbols cannot split into smaller symbols
    - $\Sigma \neq \{aa, ab, abcd\}$

$$
\begin{aligned}
    & \Sigma = \{a, b, c, d\}
\end{aligned}
$$

-   **Tip**: Alphabet is a **Set** so we can use all `set operations` on it
-   **Tip**: Alphabets often displayed with **a, b, c**

---

## Operations

There are many **Operations** available for **Alphabets**

---

### Cardinality

**Cardinality** or **Size** of an `alphabet set`, is the **Number of Symbols** in it, indicates with $|\Sigma|$

$$
\begin{aligned}
    & \Sigma = \{a, b, c\}
    \\
    \\
    & |\Sigma| = 3
\end{aligned}
$$

---

### Product

**Product** or **Cross Product** or **Cartesian Product** of two `alphabet sets`, is **Concatenation** of `each element` of `first set` with `each element` of `second set`, indicates with $\Sigma_A.\Sigma_B$

$$
\begin{aligned}
    & \Sigma_A = \{a, b, c\}
    \\
    & \Sigma_B = \{x, y\}
    \\
    \\
    & \Sigma_A.\Sigma_B =
    \{
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        x \\ y
    \end{cases}\right\}
    \} = \{ax, ay, bx, by, cx, cy\}
\end{aligned}
$$

---

### Exponent

Is **Producting** a **Set** with itself `k times`

$$
\begin{aligned}
    & \Sigma = \{a, b, c\}
    \\
    \\
    & \Sigma^0 = \{\lambda\}
    \\
    \\
    & \Sigma^1 = \Sigma =
    \{
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \} = \{a, b, c\}
    \\
    \\
    & \Sigma^2 = \Sigma.\Sigma =
    \{
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \} = \{aa, ab, ac, ba, bb, bc, ca, cb, cc\}
    \\
    \\
    & \Sigma^3 = \Sigma.\Sigma.\Sigma =
    \{
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \} = \{aaa, aab, \dots, ccb, ccc\}
    \\
    \\
    & \Sigma^k = \underbrace{\Sigma.\Sigma.\dots.\Sigma}_{\text{k times}} =
    \{
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \times
    \dots
    \times
    \left.\begin{cases}
        a \\ b \\ c
    \end{cases}\right\}
    \} = \{aa \dots a, \dots, cc \dots c\}
\end{aligned}
$$

-   **Tip**: $\Sigma^k$ contains all **Possible Strings** with size of `k`
    -   $$
        \begin{aligned}
            & \Sigma^0: \texttt{All possible string of size 0}
            \\
            & \Sigma^1: \texttt{All possible string of size 1}
            \\
            & \Sigma^2: \texttt{All possible string of size 2}
            \\
            & \Sigma^3: \texttt{All possible string of size 3}
        \end{aligned}
        $$
-   **Tip**: $|\Sigma^k| = |\Sigma|^k$:
    -   $$
        \begin{aligned}
            & |\Sigma^0| = |\Sigma|^0 = 3^0 = 1
            \\
            & |\Sigma^1| = |\Sigma|^1 = 3^1 = 3
            \\
            & |\Sigma^2| = |\Sigma|^2 = 3^2 = 9
            \\
            & |\Sigma^3| = |\Sigma|^3 = 3^3 = 27
        \end{aligned}
        $$

---

### Kleene Star

The **Union** of $\Sigma^k$ for **k** from $0$ to $\infty$

$$
\begin{aligned}
    & \Sigma^* = \bigcup_{i=0}^{\infty} \Sigma^i = \Sigma^0 \cup \Sigma^1 \cup \Sigma^2 \cup \dots
\end{aligned}
$$

-   **Tip**: $\Sigma^*$ contains all **Possible Strings** with `any size` (**Universe Set**)

---

### Kleene Plus

The **Union** of $\Sigma^k$ for **k** from $1$ to $\infty$

$$
\begin{aligned}
    & \Sigma^+ = \bigcup_{i=1}^{\infty} \Sigma^i = \Sigma^1 \cup \Sigma^2 \cup \Sigma^3 \cup \dots
    \\
    \\
    & \Sigma^+ = \Sigma^* - \{\lambda\}
    \\
    \\
    & \Sigma^+ = \Sigma.\Sigma^* = \Sigma^*.\Sigma
\end{aligned}
$$

-   **Tip**: $\Sigma^+$ contains all **Possible Strings** with `non-zero size`

---
