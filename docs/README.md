# Computational Theory

**Computational Theory** or **Theory of Computation** is the branch that deals with:

1. **which** problems **can be solved** on a **computational model**, using an algorithm
2. **how efficiently** they can be solved or to what degree

![Computational Theory](./assets/computational_theory.gif)

In this tutorial we learn about these fields:

1. **Language Theory**: Deals with the `ways of communication` of machines
2. **Automata Theory**: Deals with the `models of machines` and their power
3. **Computability Theory**: Deals with the `categories of problems` and solvability
4. **Complexity Theory**: Deals with complexity of problems and `time, memory` they need

---

## Dependencies

1. **Arithmetic**
2. **Combinatorial Theory**

---

## Language

Is a **Set** of **String** that used to `communicate` between two `machines`

There are two categories of languages:

-   **Formal Language**: `Can describe` by `mathematical` models
-   **Informal Language**: `Cannot describe` by `mathematical` models

In this tutorial we focus on `formal languages`

---

### Concepts

There are many important concepts in **Language**:

-   **Alphabet**: Set of `symbols` or `characters`
-   **String**: Sequence of `alphabets`
-   **Language**: Set of finite or infinite `strings`

---

### Categories

**Noam Chomsky** in `1956` described the **Formal Languages** categories based on the language **String Relations Complexity**

![Chomsky Hierarchy 1](./assets/language_categories_1.jpg)

| Category   | Language                   | Grammar                         | Automata                       |
| ---------- | -------------------------- | ------------------------------- | ------------------------------ |
| **Type 0** | **Recursively enumerable** | Unrestricted Grammar (UG)       | Turing Machine (TM)            |
| **Type 1** | **Context-sensitive**      | Context-Sensitive Grammar (CSG) | Linear-Bounded Automaton (LBA) |
| **Type 2** | **Context-free**           | Context-Free Grammar (CFG)      | Pushdown Automaton (PDA)       |
| **Type 3** | **Regular**                | Regular Grammar (RG)            | Finite Automaton (FA)          |

![Chomsky Hierarchy 2](./assets/language_categories_2.png)

---

## Grammar

Is a set of **Structural Rules** to describe a language

---

## Automata

Is an automatic machine that **Accepts a Language** and **Do Somthing**

---
