# Context Sensitive Language (CSL)

Is a **Formal Language** that can defined by a **Context Sensitive Grammar**

-   **Tip**: $\texttt{Context Sensitive} = \texttt{Non-Deterministic Context Sensitive}$

---

## Detection

There are **Three Ways** of detecting a language is **Context Sensitive** or not:

-   **Language**
-   **Grammar**
-   **Automata**

---

### Language

If the language has **Expression** with `any set` of `finite dependent variables` it is context-sensitive

-   **Dependent Variable**: Two or more expression variables which are related to $n$

$$
\begin{aligned}
    & \{a^n b^n c^n d^n\} \implies \{\underbrace{a^n b^n c^n d^n}_{n}\} \implies \checkmark
    \\
    & \{a^n b^n c^n\} \implies \{\underbrace{a^n b^n c^n}_{n}\} \implies \checkmark
    \\
    & \{a^p\} \implies \times
\end{aligned}
$$

---

### Grammar

If the language has **Context Sensitive Grammar** it is context-sensitive

---

### Automata

If the language has **Non-Deterministic Linear-Bounded Automata** it is context-sensitive

$$
\begin{aligned}
    & \{a^n b^n c^n d^n\} \implies \text{LBA} \implies \checkmark
    \\
    & \{a^n b^n c^n\} \implies \text{LBA} \implies \checkmark
    \\
    & \{a^p\} \implies \times
\end{aligned}
$$

---

**Example 1**: Decide which of these languages is **Context-Sensitive**:

$$
\begin{aligned}
    & L_1 = \{a^n b^n c^k : n \geq 0, n \geq k\}
    \\ \implies
    & \{\underbrace{a^n b^n c^{\leq n}}_{n}\} \implies \checkmark
    \\
    \\
    & L_2 = \{a^n b^m c^k : n \leq m \leq k\}
    \\ \implies
    & \{\underbrace{a^{\leq m} b^m c^{\geq m}}_{m}\} \implies \checkmark
    \\
    \\
    & L_3 = \{a^n b^m c^k : n = m \;OR\; m \neq k\}
    \\ \implies
    & \{\underbrace{a^n b^n}_{n} c^k\} \cup \{a^n \underbrace{b^m c^{\neq m}}_{m}\} \implies \checkmark
    \\
    \\
    & L_4 = \{w w^R : w \in \Sigma^*\}
    \\ \implies
    & \text{LBA} \implies \checkmark
    \\
    \\
    & L_5 = \{w w : w \in \Sigma^*\}
    \\ \implies
    & \text{LBA} \implies \checkmark
    \\
    \\
    & L_6 = \{a^{n^2} : n \geq 0\}
    \\ \implies
    & \text{Compute n^2} = \text{No LBA} \implies \times
    \\
    \\
    & L_7 = \{a^p : p: \text{Prime}\}
    \\ \implies
    & \text{Compute primes} = \text{No LBA} \implies \times
\end{aligned}
$$

---

## Closure Operations

There are many operations on **Context-Sensitive Languages** that the output is also **Context-Sensitive**

---

### $\overline{L}$

$$
\begin{aligned}
    & L: \text{Context-Sensitive} \iff \overline{L}: \text{Context-Sensitive}
\end{aligned}
$$

---

### $L_1 \cap L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Sensitive}
        \\
        L_2: \text{Context-Sensitive}
    \end{cases}\right\} \implies L_1 \cap L_2: \text{Context-Sensitive}
\end{aligned}
$$

---

### $L_1 \cup L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Sensitive}
        \\
        L_2: \text{Context-Sensitive}
    \end{cases}\right\} \implies L_1 \cup L_2: \text{Context-Sensitive}
\end{aligned}
$$

---

### $L_1 - L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Sensitive}
        \\
        L_2: \text{Context-Sensitive}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 - L_2: \text{Context-Sensitive}
        \\
        L_2 - L_1: \text{Context-Sensitive}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \,\,/\,\, L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Sensitive}
        \\
        L_2: \text{Anything}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \,\,/\,\, L_2: \text{Context-Sensitive}
        \\
        L_1 \setminus\, L_2: \text{Context-Sensitive}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Sensitive}
        \\
        L_2: \text{Context-Sensitive}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times L_2: \text{Context-Sensitive}
        \\
        L_2 \times L_1: \text{Context-Sensitive}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L^*$

$$
\begin{aligned}
    & L: \text{Context-Sensitive} \implies L^*: \text{Context-Sensitive}
\end{aligned}
$$

---

### $L^+$

$$
\begin{aligned}
    & L: \text{Context-Sensitive} \implies L^+: \text{Context-Sensitive}
\end{aligned}
$$

---

### $L^R$

$$
\begin{aligned}
    & L: \text{Context-Sensitive} \implies L^R: \text{Context-Sensitive}
\end{aligned}
$$

---
