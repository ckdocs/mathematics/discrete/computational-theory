# Context Sensitive Language (CSL)

Is a **Formal Language** that can defined by a **Context Sensitive Grammar**

There are two categories of **Context Sensitive Languages**:

-   **Deterministic Context Sensitive**:
    -   Can describe by **Deterministic Context Sensitive Grammar**
    -   Can accept by **DLBA**
-   **Context Sensitive**:
    -   Can describe by **Context Sensitive Grammar**
    -   Can accept by **LBA**

![Categories](../../assets/csl_categories.png)

-   **Tip**: **Context Sensitive Languages** doesn't have $\lambda$ null string
-   **Tip**: We `don't know` whether context sensitive `categories` are `equal or not`
-   $$
    \begin{aligned}
        & \text{CSL} \overset{?}{=} \text{DCSL}
    \end{aligned}
    $$

---

## CSL => Automata

Converting a **Context Sensitive Language** to **Linear-Bounded Automata** is very simple:

-   **Find** `context-sensitive expression` of language
-   **Use** regular languages `rules table` to convert `regular expressions`
-   **Use** `array` for checking `relation` between two or many `regular expressions`

---

**Example 1**: Find **Linear-Bounded Automata** for this **Context Sensitive Language**:

$$
\begin{aligned}
    & L = \{a^n b^n c^n : n \geq 0\}
\end{aligned}
$$

![Automata Example 1](../../assets/csl_to_automata_example_1.png)

---

## CSL => Grammar

There is a **Bottom-Up** approach to find a **Grammar** of a **Context Sensitive Language**:

-   **Find** the `context-sensitive expression` of language
-   **Split** language `expressions`
-   **Write** `grammar` for each part
-   **Merge** grammars together
-   **Swap** variables and `replace`

---

**Example 1**: Find **Context Sensitive Grammar** for this **Context Sensitive Language**:

$$
\begin{aligned}
    & L = \{a^n b^m c^n d^m : n,m \geq 1\}
    \\
    \\ \implies
    & L = \{a^n b^m d^m c^n : n, m \geq 1\} + \text{Swap d, c}
    \\
    \\ \implies
    & L_1 = \{a^n b^m d^m c^n : n, m \geq 1\} \implies
    \left.\begin{cases}
        S \longrightarrow aSc \;|\; aAc
        \\
        A \longrightarrow bAd \;|\; bd
    \end{cases}\right\}
    \\
    & L_2 = \text{Swap d, c} \implies
    \left.\begin{cases}
        DC \longrightarrow CD
    \end{cases}\right\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow aSC \;|\; aAC
        \\
        A \longrightarrow bAD \;|\; bD
        \\
        DC \longrightarrow CD
        \\
        \\
        \text{Replace C,D}
        \\
        bC \longrightarrow bc
        \\
        cC \longrightarrow cc
        \\
        cD \longrightarrow cd
        \\
        dD \longrightarrow dd
    \end{cases}\right\}
\end{aligned}
$$

---
