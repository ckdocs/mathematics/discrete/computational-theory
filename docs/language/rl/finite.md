# Finite Language (FL)

Each language contains **Finite** number of `strings` is **Regular**

---

## Detection

There are **Three Ways** of detecting a language is **Finite** or not:

-   **Language**
-   **Grammar**
-   **Automata**

---

### Language

If the language has **Finite** `number of strings` it is finite

---

### Grammar

If the language has **Grammar** without **Cycle**

---

### Automata

If the language has **Finite Automata** without **Loop**

-   **Finite Automata**: An automata with finite memory

---

## Closure Operations

There are many operations on **Finite Languages** that the output is also **Finite**

---

### $L_1 \cap L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Finite}
        \\
        L_2: \text{Finite}
    \end{cases}\right\} \implies L_1 \cap L_2: \text{Finite}
\end{aligned}
$$

---

### $L_1 \cup L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Finite}
        \\
        L_2: \text{Finite}
    \end{cases}\right\} \implies L_1 \cup L_2: \text{Finite}
\end{aligned}
$$

---

### $L_1 - L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Finite}
        \\
        L_2: \text{Finite}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 - L_2: \text{Finite}
        \\
        L_2 - L_1: \text{Finite}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \,\,/\,\, L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Finite}
        \\
        L_2: \text{Anything}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \,\,/\,\, L_2: \text{Finite}
        \\
        L_1 \setminus\, L_2: \text{Finite}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Finite}
        \\
        L_2: \text{Finite}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times L_2: \text{Finite}
        \\
        L_2 \times L_1: \text{Finite}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L^R$

$$
\begin{aligned}
    & L: \text{Finite} \implies L^R: \text{Finite}
\end{aligned}
$$

---

### $h(L)$

$$
\begin{aligned}
    & L: \text{Finite} \implies h(L): \text{Finite}
\end{aligned}
$$

---
