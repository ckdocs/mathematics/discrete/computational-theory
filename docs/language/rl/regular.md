# Regular Language (RL)

Is a **Formal Language** that can defined by a **Regular Grammar**

---

## Detection

There are **Three Ways** of detecting a language is **Regular** or not:

-   **Language**
-   **Grammar**
-   **Automata**

---

### Language

If the language has **Expression** `without dependent variables` it is regular

-   **Dependent Variable**: Two or more expression variables which are related to $n$

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \{\underbrace{a^n b^n}_{n} \underbrace{a^m b^m}_{m}\} \implies \times
    \\
    & \{a^n b^n\} \implies \{\underbrace{a^n b^n}_{n}\} \implies \times
    \\
    & \{a^n b^m\} \implies \{\underbrace{a^n}_{n} \underbrace{b^m}_{m}\} \implies \checkmark
\end{aligned}
$$

---

### Grammar

If the language has **Regular Grammar** it is regular

---

### Automata

If the language has **Finite Automata** it is regular

-   **Finite Automata**: An automata with finite memory

$$
\begin{aligned}
    & \{a^n\} \implies \text{FSM} \implies \checkmark
    \\
    & \{a^n b^n\} \implies \text{Needs Memory} \implies \times
\end{aligned}
$$

---

**Example 1**: Decide which of these languages is **Regular**:

$$
\begin{aligned}
    & L_1 = \{a^n b^n : n \geq 0\}
    \\ \implies
    & \{\underbrace{a^n b^n}_{n}\} \implies \times
    \\
    \\
    & L_2 = \{a^n b^{3n} : 0 \geq n \geq 2^{100}\}
    \\ \implies
    & \text{Finite} \implies \checkmark
    \\
    \\
    & L_3 = \{(ab)^n b (ab)^n : n \geq 0\}
    \\ \implies
    & \{\underbrace{(ab)^n \dots (ab)^n}_{n}\} \implies \times
    \\
    \\
    & L_4 = \{(ab)^n a (ba)^n b : n \geq 0\}
    \\ \implies
    & \{(ab)^n (ab)^n ab\} = \{\underbrace{(ab)^{2n+1}}_{n}\} \implies \checkmark
    \\
    \\
    & L_5 = \{u w w^R : u,w \in \Sigma^*\}
    \\ \implies
    & \{u \lambda \lambda\} = \Sigma^* \implies \checkmark
    \\
    \\
    & L_6 = \{w \in \Sigma^* : n_a(w) \% 2 = n_b(w) \% 2\}
    \\ \implies
    & n_a(w) \% 2 = 0,1 \in O(1) \implies \checkmark
    \\
    \\
    & L_7 = \{w \in \Sigma^* : n_a(w) = n_b(w)\}
    \\ \implies
    & n_a(w) \in O(n) \implies \times
\end{aligned}
$$

---

## Pumping Lemma

Is a lemma used to proof that a **Language** is **Not Regular**, we **Cannot** use it for proofing a language is **Regular**:

-   **Find** a **Cycle** in FA
-   **Find** a **String** that can generate by FA cycle, but not in language
-   $$
    \begin{aligned}
        & \exists w \in L(M) = x.y^i.z : w \not\in L \implies L: \text{Non-Regular}
    \end{aligned}
    $$

$$
\begin{aligned}
    & \exists c \geq 0 : \forall w \in L, |w| \geq c
    \\ \implies
    & w = xyz, |y| \geq 1, |xy| \leq c
    \\
    \\ \implies
    & \forall i \geq 0 : x.y^i.z \in L
\end{aligned}
$$

-   **Tip**: The **First State** of **Cycle** is always **Begining of $y$**
-   **Tip**: If a **DFA** with **n Steps**, accepts a string of length **|S| >= n**, it has a **Cycle**
-   **Tip**: If a **DFA** has a **Non-Empty Cycle**, the language is **Infinite**

---

**Example 1**: What is the smallest **Pumped** strings that DFA can accept?

![Pumping Lemma Example 1](../../assets/rl_pumping_lemma_example_1.png)

$$
\begin{aligned}
    & x.y^1.z = abc
\end{aligned}
$$

---

**Example 2**: What is the smallest **Pumped** strings that DFA can accept?

![Pumping Lemma Example 2](../../assets/rl_pumping_lemma_example_2.png)

$$
\begin{aligned}
    & x.y^1.z = a = b
\end{aligned}
$$

---

## Closure Operations

There are many operations on **Regular Languages** that the output is also **Regular**

---

### $\overline{L}$

$$
\begin{aligned}
    & L: \text{Regular} \iff \overline{L}: \text{Regular}
\end{aligned}
$$

-   **Tip**: $DFA(L): \text{Minimized}$ $\iff$ $DFA(\overline{L}): \text{Minimized}$

---

**Proof 1**: By introducing **FA** of $\overline{L}$:

$$
\begin{aligned}
    & M = (Q, \Sigma, \delta, q_0, F)
    \\
    \\ \implies
    & \overline{M} = (Q, \Sigma, \delta, q_0, Q-F)
\end{aligned}
$$

-   **Change** `final states` to `non-final state`
-   **Change** `non-final states` to `final state`

![Complement Proof 1](../../assets/rl_complement_proof_1.png)

---

### $L_1 \cap L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Regular}
        \\
        L_2: \text{Regular}
    \end{cases}\right\} \implies L_1 \cap L_2: \text{Regular}
\end{aligned}
$$

---

**Proof 1**: By introducing **FA** of $L_1 \cap L_2$:

$$
\begin{aligned}
    & M_1 = (Q, \Sigma, \delta_1, q_0, F_1)
    \\
    & M_2 = (P, \Sigma, \delta_2, p_0, F_2)
    \\
    \\ \implies
    & M_1 \cap M_2 = (Q \times P, \Sigma, [\delta_1, \delta_2], [q_0, p_0], F_1 \cap F_2)
\end{aligned}
$$

-   **Create** DFA of $M_1 \times M_2$ with both automatons rules
-   **Set** states as **Final** which **q** and **p** are final

![Intersection Proof 1](../../assets/rl_intersection_proof_1.png)

---

**Proof 2**: By using **De Morgan's** law:

-   **Assume** that $\overline{X}$ is regular
-   **Assume** that $X \cup Y$ is regular

$$
\begin{aligned}
    & L_1 \cap L_2 = \overline{\overline{L_1 \cap L_2}} = \overline{\overline{L_1} \cup \overline{L_2}}
\end{aligned}
$$

---

### $L_1 \cup L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Regular}
        \\
        L_2: \text{Regular}
    \end{cases}\right\} \implies L_1 \cup L_2: \text{Regular}
\end{aligned}
$$

---

**Proof 1**: By introducing **FA** of $L_1 \cup L_2$:

$$
\begin{aligned}
    & M_1 = (Q, \Sigma, \delta_1, q_0, F_1)
    \\
    & M_2 = (P, \Sigma, \delta_2, p_0, F_2)
    \\
    \\ \implies
    & M_1 \cup M_2 = (Q \times P, \Sigma, [\delta_1, \delta_2], [q_0, p_0], F_1 \cup F_2)
\end{aligned}
$$

-   **Create** DFA of $M_1 \times M_2$ with both automatons rules
-   **Set** states as **Final** which **q** or **p** are final

![Union Proof 1](../../assets/rl_union_proof_1.png)

---

**Proof 2**: By introducing **FA** of $L_1 \cup L_2$:

-   **Create** an state called **S**
-   **Connect** new start state to machines start state with $\lambda$

![Union Proof 2](../../assets/rl_union_proof_2.png)

---

**Proof 3**: By using **De Morgan's** law:

-   **Assume** that $\overline{X}$ is regular
-   **Assume** that $X \cap Y$ is regular

$$
\begin{aligned}
    & L_1 \cup L_2 = \overline{\overline{L_1 \cup L_2}} = \overline{\overline{L_1} \cap \overline{L_2}}
\end{aligned}
$$

---

### $L_1 - L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Regular}
        \\
        L_2: \text{Regular}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 - L_2: \text{Regular}
        \\
        L_2 - L_1: \text{Regular}
    \end{cases}\right\}
\end{aligned}
$$

---

**Proof 1**: By introducing **FA** of $L_1 - L_2$:

$$
\begin{aligned}
    & M_1 = (Q, \Sigma, \delta_1, q_0, F_1)
    \\
    & M_2 = (P, \Sigma, \delta_2, p_0, F_2)
    \\
    \\ \implies
    & M_1 - M_2 = (Q \times P, \Sigma, [\delta_1, \delta_2], [q_0, p_0], F_1 - F_2)
\end{aligned}
$$

-   **Create** DFA of $M_1 \times M_2$ with both automatons rules
-   **Set** states as **Final** which **q** is final **p** is non-final

![Difference Proof 1](../../assets/rl_difference_proof_1.png)

---

**Proof 2**: By using **De Morgan's** law:

-   **Assume** that $\overline{X}$ is regular
-   **Assume** that $X \cap Y$ is regular

$$
\begin{aligned}
    & L_1 - L_2 = L_1 \cap \overline{L_2}
\end{aligned}
$$

---

### $L_1 \,\,/\,\, L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Regular}
        \\
        L_2: \text{Anything}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \,\,/\,\, L_2: \text{Regular}
        \\
        L_1 \setminus\, L_2: \text{Regular}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Regular}
        \\
        L_2: \text{Regular}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times L_2: \text{Regular}
        \\
        L_2 \times L_1: \text{Regular}
    \end{cases}\right\}
\end{aligned}
$$

---

**Proof 1**: By introducing **FA** of $L_1 \times L_2$:

$$
\begin{aligned}
    & M_1 = (Q, \Sigma, \delta_1, q_0, F_1)
    \\
    & M_2 = (P, \Sigma, \delta_2, p_0, F_2)
    \\
    \\ \implies
    & M_1 \times M_2 = (Q + P, \Sigma, \delta_1 \cup \delta_2, q_0, F_2)
\end{aligned}
$$

-   **Connect** `final states` of $L_1$ with $\lambda$ to `start state` of $L_2$

![Production Proof 1](../../assets/rl_production_proof_1.png)

---

### $L^*$

$$
\begin{aligned}
    & L: \text{Regular} \implies L^*: \text{Regular}
\end{aligned}
$$

---

**Proof 1**: By introducing **FA** of $L^*$:

$$
\begin{aligned}
    & M = (Q, \Sigma, \delta, q_0, F)
    \\
    \\ \implies
    & M^* = (Q, \Sigma, \delta^{'}, q_0, F)
\end{aligned}
$$

-   **Connect** `final states` with $\lambda$ to `start state`

![Kleene Star Proof 1](../../assets/rl_kleene_star_proof_1.png)

---

### $L^+$

$$
\begin{aligned}
    & L: \text{Regular} \implies L^+: \text{Regular}
\end{aligned}
$$

---

**Proof 1**: By using **Kleene** rules:

-   **Assume** that $X^*$ is regular
-   **Assume** that $X.Y$ is regular

$$
\begin{aligned}
    & L^+ = L.L^*
\end{aligned}
$$

---

### $L^R$

$$
\begin{aligned}
    & L: \text{Regular} \implies L^R: \text{Regular}
\end{aligned}
$$

-   **Tip**: $MinDFA\_States(L): n$ $\implies$ $MinDFA\_States(L^R): [\log{n}, 2^n]$

---

**Proof 1**: By introducing **FA** of $L^R$:

$$
\begin{aligned}
    & M = (Q, \Sigma, \delta, q_0, F)
    \\
    \\ \implies
    & M^R = (Q, \Sigma, \delta^R, F, q_0)
\end{aligned}
$$

-   **Change** `final states` to `start state`
-   **Change** `start states` to `final state`
-   **Reverse** `edges` and their `label`

![Reversal Proof 1](../../assets/rl_reversal_proof_1.png)

---

### $h(L)$

$$
\begin{aligned}
    & L: \text{Regular} \implies h(L): \text{Regular}
\end{aligned}
$$

---
