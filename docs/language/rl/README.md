# Regular Language (RL)

Is a **Formal Language** that can defined by a **Regular Grammar**

There are two categories of **Regular Languages**:

-   **Finite**: Language contains `finite` number of `strings`
-   **Regular**: Infinite languages that can described by a `regular grammar`

![Categories](../../assets/rl_categories.png)

---

## RL => Automata

Converting a **Regular Language** to **Finite Automata** is very simple:

-   **Find** `regular expression` of language
-   **Use** the `rules table` and convert `regular expression` to `NFA`

![Automata](../../assets/rl_to_automata.gif)

---

**Example 1**: Find **Finite Automata** for this **Regular Language**:

$$
\begin{aligned}
    & L = \{w \in \Sigma^* : n_0(w) \;mod\; 2 \neq n_1(w) \;mod\; 2\}
\end{aligned}
$$

![Automata Example 1](../../assets/rl_to_automata_example_1.png)

---

**Example 2**: Find **Finite Automata** for this **Regular Language**:

$$
\begin{aligned}
    & L = (a+b)^*.abb
\end{aligned}
$$

![Automata Example 2](../../assets/rl_to_automata_example_2.png)

---

## RL => Grammar

There is a **Bottom-Up** approach to find a **Grammar** of a **Regular Language**:

-   **Find** the `regular expression` of language
-   **Split** language `expressions`
-   **Write** `grammar` for each part using `rules table`
-   **Merge** grammars together

![Grammar](../../assets/rl_to_grammar.png)

There are some useful partitions and their grammars:

-   $(\Sigma^k)^*$ $\implies$ $S \longrightarrow \{\text{Permutations of k symbol}\}S$

---

**Example 1**: Find **Regular Grammar** for this **Regular Language**:

$$
\begin{aligned}
    & L = \{abb\}.\{a,b\}^*.\{bba\}
    \\
    \\ \implies
    & L_1 = \{abb\} \implies
    \left.\begin{cases}
        A \longrightarrow abb
    \end{cases}\right\}
    \\
    & L_2 = \{a,b\}^* \implies
    \left.\begin{cases}
        B \longrightarrow aC \;|\; bC \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_3 = \{bba\} \implies
    \left.\begin{cases}
        C \longrightarrow bba
    \end{cases}\right\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow ABC
        \\
        A \longrightarrow abb
        \\
        B \longrightarrow aC \;|\; bC \;|\; \lambda
        \\
        C \longrightarrow bba
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 2**: Find **Regular Grammar** for this **Regular Language**:

$$
\begin{aligned}
    & L = \{w \in \{a,b\}^* : |w| \;mod\; 3 = 0\}
    \\
    \\ \implies
    & L = (\Sigma^3)^* = (\{a,b\}^3)^*
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow aaaS \;|\; aabS \;|\; abaS \;|\; abbS
        \\
        S \longrightarrow baaS \;|\; babS \;|\; bbaS \;|\; bbbS \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 3**: Find **Regular Grammar** for this **Regular Language**:

$$
\begin{aligned}
    & L = \{w \in \{a,b\}^* : \text{w contains 3 a}\}
    \\
    \\ \implies
    & L = b^*.a.b^*.a.b^*.a.b^*
    \\
    \\ \implies
        & L_1 = \{b^*\} \implies
    \left.\begin{cases}
        A \longrightarrow bA \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_2 = \{a\} \implies
    \left.\begin{cases}
        B \longrightarrow a
    \end{cases}\right\}
    \\
    & L_3 = \{b^*\} \implies
    \left.\begin{cases}
        C \longrightarrow bC \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_4 = \{a\} \implies
    \left.\begin{cases}
        D \longrightarrow a
    \end{cases}\right\}
    \\
    & L_5 = \{b^*\} \implies
    \left.\begin{cases}
        E \longrightarrow bE \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_6 = \{a\} \implies
    \left.\begin{cases}
        F \longrightarrow a
    \end{cases}\right\}
    \\
    & L_6 = \{b^*\} \implies
    \left.\begin{cases}
        G \longrightarrow bG \;|\; \lambda
    \end{cases}\right\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow ABCDEFG
        \\
        A \longrightarrow bA \;|\; \lambda
        \\
        B \longrightarrow a
        \\
        C \longrightarrow bC \;|\; \lambda
        \\
        D \longrightarrow a
        \\
        E \longrightarrow bE \;|\; \lambda
        \\
        F \longrightarrow a
        \\
        G \longrightarrow bG \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 4**: Find **Regular Grammar** for this **Regular Language**:

$$
\begin{aligned}
    & L = \{w \in \{a,b\}^* : \text{w without aba}\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow bS \;|\; aA \;|\; \lambda
        \\
        A \longrightarrow aA \;|\ bB
        \\
        B \longrightarrow bB \;|\ \lambda
    \end{cases}\right\}
\end{aligned}
$$

---
