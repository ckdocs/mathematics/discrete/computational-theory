# Recursively Enumerable Language (REL)

Is a **Formal Language** that can defined by an **Unrestricted Grammar**

There are two categories of **Recursively Enumerable Languages**:

-   **Recursive**:
    -   Can describe by **Unrestricted Grammar**
    -   Can **Decide** by **Turing Machine**
-   **Recursively Enumerable**:
    -   Can describe by **Unrestricted Grammar**
    -   Can **Recognize** by **Turing Machine**

![Categories](../../assets/rel_categories.png)

---

## Reduction

Is an algorithm used to transform a problem into a simpler problem to detect that it's **Decidable** or **Recognizable**

$$
\begin{aligned}
    & A \ll B
    \\
    & \texttt{A reduced to B}
    \\
    \\
    & B: \text{Decidable} \implies A: \text{Decidable}
    \\
    & A: \text{Non-Decidable} \implies B: \text{Non-Decidable}
    \\
    & B: \text{Recognizable} \implies A: \text{Recognizable}
    \\
    & A: \text{Non-Recognizable} \implies B: \text{Non-Recognizable}
\end{aligned}
$$

---

## Acceptable

There are two categories of languages:

-   **Turing Decidable**: A language that we can find an `acceptor turing machine` that **Always Halt**
-   **Turing Recognizable**: A language that we can find an `acceptor turing machine` that **Halt or Loops**

![Acceptable](../../../assets/tm_acceptable.png)

-   **Tip**: Every **Decider** is a **Recognizer**
-   **Tip**: For **Turing Decidable** languages we have **String Existence** algorithm
-   **Tip**: We cannot find a **Algorithm** for **Undecidable** problems
    -   Algorithms must **Halt**

---

## REL => Automata

Converting a **Recursively Enumerable Language** to **Turing Machine** is very simple:

-   **Find** `unrestricted expression` of language
-   **Use** regular languages `rules table` to convert `regular expressions`
-   **Use** `infinite array` for checking `relation` between two or many `regular expressions`

---

**Example 1**: Find **Turing Machine** for this **Recursively Enumerable Language**:

$$
\begin{aligned}
    & L = \{a^n b^n c^n : n \geq 0\}
\end{aligned}
$$

![Automata Example 1](../../assets/rel_to_automata_example_1.png)

---

## REL => Grammar

There is a **Bottom-Up** approach to find a **Grammar** of a **Recursively Enumerable Language**:

-   **Find** the `unrestricted expression` of language
-   **Split** language `expressions`
-   **Write** `grammar` for each part
-   **Merge** grammars together
-   **Swap** variables and `replace`

---

**Example 1**: Find **Unrestricted Grammar** for this **Recursively Enumerable Language**:

$$
\begin{aligned}
    & L = \{a^n b^m c^n d^m : n,m \geq 1\}
    \\
    \\ \implies
    & L = \{a^n b^m d^m c^n : n, m \geq 1\} + \text{Swap d, c}
    \\
    \\ \implies
    & L_1 = \{a^n b^m d^m c^n : n, m \geq 1\} \implies
    \left.\begin{cases}
        S \longrightarrow aSc \;|\; aAc
        \\
        A \longrightarrow bAd \;|\; bd
    \end{cases}\right\}
    \\
    & L_2 = \text{Swap d, c} \implies
    \left.\begin{cases}
        DC \longrightarrow CD
    \end{cases}\right\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow aSC \;|\; aAC
        \\
        A \longrightarrow bAD \;|\; bD
        \\
        DC \longrightarrow CD
        \\
        \\
        \text{Replace C,D}
        \\
        bC \longrightarrow bc
        \\
        cC \longrightarrow cc
        \\
        cD \longrightarrow cd
        \\
        dD \longrightarrow dd
    \end{cases}\right\}
\end{aligned}
$$

---
