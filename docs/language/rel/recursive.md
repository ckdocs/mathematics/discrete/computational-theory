# Recursive Language (REC)

Is a **Formal Language** which there exists a **Turing Machine** which will **Halt and Accept** all the strings `in language` and **Halt and Reject** all the strings `not in language`

-   **Tip**: $\texttt{Recursive} = \texttt{Decidable}$
-   **Tip**: $\texttt{Recursive} \implies \texttt{Decider Turing Machine}$

---

## Detection

There is **One Way** of detecting a language is **Recursive** or not:

-   **Automata**

---

### Automata

If the language has **Decider Turing Machine** it is recursive

$$
\begin{aligned}
    & \{a^n b^n c^n d^n\} \implies \text{TM} \implies \checkmark
    \\
    & \{a^n b^n c^n\} \implies \text{TM} \implies \checkmark
    \\
    & \{a^p\} \implies \text{TM} \implies \checkmark
\end{aligned}
$$

---

**Example 1**: There are some **Undecidable** problems:

$$
\begin{aligned}
    & \left.\begin{cases}
        M_1: \text{TM}
        \\
        M_2: \text{TM}
    \end{cases}\right\}
    \overset{\text{Undecidable}}{\implies}
    \begin{cases}
        L(M_1) \overset{?}{=} \emptyset
        \\
        L(M_1) \overset{?}{=} \text{Finite}
        \\
        L(M_1) \overset{?}{=} L(M_2)
        \\
        L(M_1) \overset{?}{\subseteq} L(M_2)
        \\
        L(M_1) \cap L(M_2) \overset{?}{=} \emptyset
    \end{cases}
    \\
    \\
    & \left.\begin{cases}
        G_1: \text{UG}
        \\
        G_2: \text{UG}
    \end{cases}\right\}
    \overset{\text{Undecidable}}{\implies}
    \begin{cases}
        L(G_1) \overset{?}{=} \emptyset
        \\
        L(G_1) \overset{?}{=} \text{Finite}
        \\
        L(G_1) \overset{?}{=} L(G_2)
        \\
        L(G_1) \overset{?}{\subseteq} L(G_2)
        \\
        L(G_1) \cap L(G_2) \overset{?}{=} \emptyset
    \end{cases}
    \\
    \\
    & \left.\begin{cases}
        M_1: \text{NPDA}
        \\
        M_2: \text{NPDA}
    \end{cases}\right\}
    \overset{\text{Undecidable}}{\implies}
    \begin{cases}
        L(M_1) \overset{?}{=} L(M_2)
        \\
        L(M_1) \overset{?}{\subseteq} L(M_2)
        \\
        L(M_1) \cap L(M_2) \overset{?}{=} \emptyset
    \end{cases}
    \\
    \\
    & \left.\begin{cases}
        G_1: \text{CFG}
        \\
        G_2: \text{CFG}
    \end{cases}\right\}
    \overset{\text{Undecidable}}{\implies}
    \begin{cases}
        L(G_1) \overset{?}{=} L(G_2)
        \\
        L(G_1) \overset{?}{\subseteq} L(G_2)
        \\
        L(G_1) \cap L(G_2) \overset{?}{=} \emptyset
    \end{cases}
    \\
    \\
    & \left.\begin{cases}
        G_1: \text{CFG}
        \\
        G_2: \text{RG}
    \end{cases}\right\}
    \overset{\text{Undecidable}}{\implies}
    \begin{cases}
        L(G_1) \overset{?}{\subseteq} L(G_2)
    \end{cases}
\end{aligned}
$$

---

**Example 2**: The **Halting Problem** is an undecidable problem

-   **Halting Problem**: Find a **Decider TM** that inputs `another TM` and `a string` and tells the virtualized TM will **Halt or Not**

$$
\begin{aligned}
    & M = \{<M^{\prime}, w> : (M^{\prime}(w) = Halt \;?\; Yes : No)\}
    \\
    & \texttt{We cannot find such decider TM}
\end{aligned}
$$

---

**Example 3**: The **Post Correspondence Problem** is an undecidable problem

---

## Closure Operations

There are many operations on **Recursive Languages** that the output is also **Recursive**

---

### $\overline{L}$

$$
\begin{aligned}
    & L: \text{Recursive} \iff \overline{L}: \text{Recursive}
\end{aligned}
$$

-   **Tip**: $L, \overline{L}: \text{R.E}$ $\implies$ $L, \overline{L}: \text{REC}$

---

### $L_1 \cap L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursive}
        \\
        L_2: \text{Recursive}
    \end{cases}\right\} \implies L_1 \cap L_2: \text{Recursive}
\end{aligned}
$$

---

### $L_1 \cup L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursive}
        \\
        L_2: \text{Recursive}
    \end{cases}\right\} \implies L_1 \cup L_2: \text{Recursive}
\end{aligned}
$$

---

### $L_1 - L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursive}
        \\
        L_2: \text{Recursive}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 - L_2: \text{Recursive}
        \\
        L_2 - L_1: \text{Recursive}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \,\,/\,\, L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursive}
        \\
        L_2: \text{Anything}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \,\,/\,\, L_2: \text{Recursive}
        \\
        L_1 \setminus\, L_2: \text{Recursive}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursive}
        \\
        L_2: \text{Recursive}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times L_2: \text{Recursive}
        \\
        L_2 \times L_1: \text{Recursive}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L^*$

$$
\begin{aligned}
    & L: \text{Recursive} \implies L^*: \text{Recursive}
\end{aligned}
$$

---

### $L^+$

$$
\begin{aligned}
    & L: \text{Recursive} \implies L^+: \text{Recursive}
\end{aligned}
$$

---

### $L^R$

$$
\begin{aligned}
    & L: \text{Recursive} \implies L^R: \text{Recursive}
\end{aligned}
$$

---

### $h^{-1}(L)$

$$
\begin{aligned}
    & L: \text{Recursive} \implies h^{-1}(L): \text{Recursive}
\end{aligned}
$$

---
