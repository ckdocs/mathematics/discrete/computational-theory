# Recursively Enumerable Language (R.E)

Is a **Formal Language** which there exists a **Turing Machine** which will **Halt and Accept** all the strings `in language` and **Halt and Reject** or **Go to Loop** for all the strings `not in language`

-   **Tip**: $\texttt{Recursively Enumerable} = \texttt{Partially Decidable} = \texttt{Recognizable}$
-   **Tip**: $\texttt{Recursively Enumerable} \implies \texttt{Recognizer Turing Machine}$

---

## Detection

There is **One Way** of detecting a language is **Recursively Enumerable** or not:

-   **Automata**

---

### Automata

If the language has **Recognizer Turing Machine** it is recursively enumerable

$$
\begin{aligned}
    & \{a^n b^n c^n d^n\} \implies \text{TM} \implies \checkmark
    \\
    & \{a^n b^n c^n\} \implies \text{TM} \implies \checkmark
    \\
    & \{a^p\} \implies \text{TM} \implies \checkmark
\end{aligned}
$$

---

## Closure Operations

There are many operations on **Recursively Enumerable Languages** that the output is also **Recursively Enumerable**

---

### $L_1 \cap L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursively Enumerable}
        \\
        L_2: \text{Recursively Enumerable}
    \end{cases}\right\} \implies L_1 \cap L_2: \text{Recursively Enumerable}
\end{aligned}
$$

---

### $L_1 \cup L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursively Enumerable}
        \\
        L_2: \text{Recursively Enumerable}
    \end{cases}\right\} \implies L_1 \cup L_2: \text{Recursively Enumerable}
\end{aligned}
$$

---

### $L_1 \,\,/\,\, L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursively Enumerable}
        \\
        L_2: \text{Anything}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \,\,/\,\, L_2: \text{Recursively Enumerable}
        \\
        L_1 \setminus\, L_2: \text{Recursively Enumerable}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Recursively Enumerable}
        \\
        L_2: \text{Recursively Enumerable}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times L_2: \text{Recursively Enumerable}
        \\
        L_2 \times L_1: \text{Recursively Enumerable}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L^*$

$$
\begin{aligned}
    & L: \text{Recursively Enumerable} \implies L^*: \text{Recursively Enumerable}
\end{aligned}
$$

---

### $L^+$

$$
\begin{aligned}
    & L: \text{Recursively Enumerable} \implies L^+: \text{Recursively Enumerable}
\end{aligned}
$$

---

### $L^R$

$$
\begin{aligned}
    & L: \text{Recursively Enumerable} \implies L^R: \text{Recursively Enumerable}
\end{aligned}
$$

---

### $h(L)$

$$
\begin{aligned}
    & L: \text{Recursively Enumerable} \implies h(L): \text{Recursively Enumerable}
\end{aligned}
$$

---

### $h^{-1}(L)$

$$
\begin{aligned}
    & L: \text{Recursively Enumerable} \implies h^{-1}(L): \text{Recursively Enumerable}
\end{aligned}
$$

---
