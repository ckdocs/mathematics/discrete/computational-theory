# Language

Any `finite` or `infinite` **Subset** of $\Sigma^*$

$$
\begin{aligned}
    & L_1 = \{ab, aaa, abc\} \subseteq \Sigma^*
    \\
    \\
    & L_2 = \{w \in \Sigma^* : |w|_a = |w|_b\} \subseteq \Sigma^*
\end{aligned}
$$

-   **Tip**: Language is a **Set** so we can use all `set operations` on it
-   **Tip**: Languages often displayed with **L**

---

## Concepts

There are many **Concepts** in **Languages**

---

### Describe

There are **Three Ways** to **Describe** a language:

-   **Set**: Defining a language using a `set`
    -   $$
        \begin{aligned}
            & L_1 = \{w : w \in \{a,b\}^*, |w| \;mod\; 3 = 0\}
            \\
            & L_2 = \{w : w \in \Sigma^*, |w| \;mod\; 3 \neq 0\}
        \end{aligned}
        $$
-   **Description**: Describing a `language properties`
    -   $$
        \begin{aligned}
            & L_1 = \texttt{Strings with size multiplication of 3}
            \\
            & L_2 = \texttt{Strings with size not multiplication of 3}
        \end{aligned}
        $$
-   **Expression**: Showing a language using `expressions`
    -   $$
        \begin{aligned}
            & L_1 = (\Sigma^3)^* = \overbrace{(\Sigma^3)^0}^{\Sigma^0} + \overbrace{(\Sigma^3)^1}^{\Sigma^3} + \overbrace{(\Sigma^3)^2}^{\Sigma^6} \dots
            \\
            & L_2 = \Sigma.(\Sigma^3)^* + \Sigma^2.(\Sigma^3)^*
        \end{aligned}
        $$

---

#### Expression

Is a way for **Describing** languages

There are three **Basic Expressions**:

-   $r = \emptyset$ $\implies$ $L(r) = \{\}$
-   $r = \lambda$ $\implies$ $L(r) = \{\lambda\}$
-   $r = a \in \Sigma$ $\implies$ $L(r) = \{a\}$

We can create **Compound Expressions** by **Operating** on **Basic Expressions**:

-   $r = r_1 + r_2$ $\implies$ $L(r) = L(r_1) \cup L(r_2)$
-   $r = r_1.r_2$ $\implies$ $L(r) = L(r_1) \times L(r_2)$
-   $r = r_1^*$ $\implies$ $L(r) = L(r_1)^*$
-   $r = r_1^+$ $\implies$ $L(r) = L(r_1)^+$
-   $r = (r_1)$ $\implies$ $L(r) = L(r_1)$

We can run all **Language Operations** on **Expressions**

---

**Example 1**: $L = \{w \in \Sigma^* : \texttt{w contains bbb}\}$

$$
\begin{aligned}
    & r = (a+b)^*.bbb.(a+b)^*
\end{aligned}
$$

---

**Example 2**: $L = \{w \in \Sigma^* : w = a^{4n+2}, n \geq 0\}$

$$
\begin{aligned}
    & r = (aaaa)^*.aa
\end{aligned}
$$

---

**Example 3**: $L = \{w \in \Sigma^* : |w| \;mod\; 3 = 0\}$

$$
\begin{aligned}
    & r = ((a+b)^3)^* = (aaa + aab + aba + abb + baa + bab + bba + bbb)^*
\end{aligned}
$$

---

**Example 4**: $L = \{w \in \Sigma^* : \texttt{w contains exactly one aa}\}$

$$
\begin{aligned}
    & r = (a+b)^*.aa.(a+b)^*
\end{aligned}
$$

---

**Example 5**: $L = \{w \in \Sigma^* : \texttt{w without aaa}\}$

$$
\begin{aligned}
    & r = (b+ab+aab)^*
\end{aligned}
$$

---

**Example 6**: $L = \{w \in \Sigma^* : \texttt{w contains exactly one aa}\}$

$$
\begin{aligned}
    & r = (b+ab)^*.aa.(b+ab)^*
\end{aligned}
$$

---

### Exist

We can check strings are existed in $L^*$ or not using **Split Tree**

1. Root of the tree is $\lambda$
2. Start from **Left to Right**
3. Try to find the **Maching Strings** of language $L$

$$
\begin{aligned}
    & L = \{ab, ba, aa, aab\}
    \\
    \\
    & \texttt{babaaaaa}
    \\
    & \lambda -
    \begin{cases}
        ba -
        \begin{cases}
            ba -
            \begin{cases}
                aa -
                \begin{cases}
                    aa \checkmark
                \end{cases}
            \end{cases}
        \end{cases}
    \end{cases}
    \\
    \\
    & \texttt{aabaaa}
    \\
    & \lambda -
    \begin{cases}
        aa -
        \begin{cases}
            ba -
            \begin{cases}
                aa \checkmark
            \end{cases}
        \end{cases}
        \\
        aab -
        \begin{cases}
            aa \times
        \end{cases}
    \end{cases}
    \\
    \\
    & \texttt{aaababaaa}
    \\
    & \lambda -
    \begin{cases}
        aa -
        \begin{cases}
            ab -
            \begin{cases}
                ab -
                \begin{cases}
                    aa \times
                \end{cases}
            \end{cases}
        \end{cases}
    \end{cases}
    \\
    \\
    & \texttt{aabaabab}
    \\
    & \lambda -
    \begin{cases}
        aa -
        \begin{cases}
            ba -
            \begin{cases}
                ab -
                \begin{cases}
                    ab \checkmark
                \end{cases}
            \end{cases}
        \end{cases}
        \\
        aab -
        \begin{cases}
            aa -
            \begin{cases}
                ba \times
            \end{cases}
            \\
            aab -
            \begin{cases}
                ab \checkmark
            \end{cases}
        \end{cases}
    \end{cases}
\end{aligned}
$$

---

### Empty

There is an **Empty Language** with size `0`, indicates with $\emptyset$

$$
\begin{aligned}
    & L = \emptyset = \{\}
    \\
    & |L| = 0
\end{aligned}
$$

-   **Tip**: Empty language is **Not Equals** to language with **Empty String**:
    -   $$
        \begin{aligned}
            & L_1 = \emptyset = \{\} \implies \texttt{Empty language}
            \\ \implies
            & |L_1| = 0
            \\
            \\
            & L_2 = \{\lambda\} \implies \texttt{Language contains empty string}
            \\ \implies
            & |L_2| = 1
        \end{aligned}
        $$

---

### Partition

We can **Parition** a `language set` into multiple **Disjoint Sets** for better understanding

$$
\begin{aligned}
    & L = \overline{\{a^n.b^n : n \geq 0\}} = \Sigma^* - \{a^n.b^n : n \geq 0\}
    \\
    & L = \{\Sigma^* - a^*.b^*\} \cup \{a^n.b^m : n \neq m\}
\end{aligned}
$$

---

### Relation

Now we try to find the relation between $L, L^+, L^*, \Sigma^*$, **L** is always `subset of other`, but in some situations these **Subsets** will be **Equals**

$$
\begin{aligned}
    & L \subset L^+ \subset L^* \subset \Sigma^*
\end{aligned}
$$

-   $\lambda \in L$ $\implies$ $L^+ = L^*$
-   $\Sigma \subset L$ $\implies$ $L^* = \Sigma^*$
-   **Tip**: If **L** contains all alphabets of sizes **All Prime Numbers**, we can generate any string of any length, except **1**

$$
\begin{aligned}
    \text{Example}:
    \\
    & L_1 = \{w \in \Sigma^* : |w| \;mod\; 2 = 1\}
    \\
    & L_1 = \Sigma^1 \cup \Sigma^3 \cup \Sigma^5 \cup \dots = \Sigma.(\Sigma^2)^*
    \\
    & L_1 = \texttt{Strings with odd size}
    \\
    & L_1 = \{a, b, aaa, aab, aba, \dots\}
    \\
    \\
    & \lambda \notin L \implies L^+ \subsetneq L^*
    \\
    & \Sigma \subset L \implies L^* = \Sigma^*
    \\
    & L^+ = \underbrace{L^1}_{\Sigma.(\Sigma^2)^*} \cup \underbrace{L^2}_{(\Sigma^2)^*} \cup \underbrace{L^3}_{\Sigma.(\Sigma^2)^*} \cup \dots \implies L \subsetneq L^+
    \\
    \\ \implies
    & L_1 \subsetneq L_1^+ \subsetneq L_1^* = \Sigma^*
    \\
    \\
    \text{Example}:
    \\
    & L_2 = \{w \in \Sigma^* : |w| \in Prime\}
    \\
    & L_2 = \texttt{Strings with prime size}
    \\
    & L_2 = \Sigma^2 \cup \Sigma^3 \cup \Sigma^5 \cup \Sigma^7 \cup \dots
    \\
    \\
    & \lambda \notin L \implies L^+ \subsetneq L^*
    \\
    & \Sigma \nsubseteq L \implies L^* \subsetneq \Sigma^*
    \\
    \\ \implies
    & L_2 \subsetneq L_2^+ \subsetneq L_2^* \subsetneq \Sigma^*
\end{aligned}
$$

---

### Ambiguity

-   **Ambiguous** language is a language that we `cannot find` any **Non-Ambiguous** grammar for it
-   **Non-Ambiguous** language is a language that we `can find` at least one **Non-Ambiguous** grammar for it

There is a way to detect a language is **Ambiguous** or not:

-   **Find** the `expression` of language
-   **Find** a `point` at `expression` which: (**Decision after infinite symbols**)
    -   We have accepted **n Symbols** from start
    -   We can have **More that One Choices**
    -   We have some **Restrictions** on **Choices**

There are some tips about **Ambiguous Languages**:

-   **Tip**:
    -   $$
        \begin{aligned}
            & \left.\begin{cases}
                L_1: \text{Un-Ambiguous}
                \\
                L_2: \text{Un-Ambiguous}
            \end{cases}\right\} \implies
            \left.\begin{cases}
                L_1 \cap L_2: \text{Ambiguous} \;|\; \text{Ambiguous}
                \\
                L_1 \cup L_2: \text{Ambiguous} \;|\; \text{Ambiguous}
            \end{cases}\right\}
            \\
            & L: \text{Un-Ambiguous} \implies L^R: \text{Un-Ambiguous}
            \\
            & L: \text{Ambiguous} \implies L^R: \text{Ambiguous}
        \end{aligned}
        $$

---

**Example 1**: Check the language bellow is **Ambiguous** or not

$$
\begin{aligned}
    & L = \{a^n b^n c^m\} \cup \{a^n b^m c^m\}
    \\
    \\ \implies
    & a^n \mapsto
    \left.\begin{cases}
        b^n \;?
        \\
        b^m \;?
    \end{cases}\right\} \implies \text{Ambiguous}
\end{aligned}
$$

---

### Closure

Closure operations for **Language Categories**

![Closure](../assets/language_closure.png)

---

## Operations

There are many **Operations** available for **Languages**

---

### $|L|$

**Cardinality** or **Size** of a `language set`, is the **Number of Strings** in it, indicates with $|L|$

$$
\begin{aligned}
    & L_1 = \{ab, aaa, abc\}
    \\
    & |L_1| = 3
    \\
    \\
    & L_2 = \{w \in \Sigma^* : |w|_a = |w|_b\}
    \\
    & |L_2| = \infty
\end{aligned}
$$

---

### $\overline{L}$

**Complement** of a `language set`, is the **Elements Not in Set**, indicates with $\overline{L}$

$$
\begin{aligned}
    & \overline{L} = L^C = \Sigma^* - L = \{u : u \not\in L\} = \{u : u \in \Sigma^* - L\}
    \\
    \\
    \texttt{Example}:
    \\
    & L = \{w : w \in \Sigma^* \;\&\; |w| \;mod\; 2 = 0\}
    \\
    & \overline{L} = \{w : w \in \Sigma^* \;\&\; |w| \;mod\; 2 = 1\}
\end{aligned}
$$

-   **Tip**: $L: \text{Finite}$ $\implies$ $\overline{L}: \text{Infinite}$
-   **Tip**: $L: \text{Infinite}$ $\implies$ $\overline{L}: \text{Finite or Infinite}$
-   **Tip**: $L = \emptyset$ $\implies$ $\overline{L} = \Sigma^*$
-   **Tip**: $L = \{\lambda\}$ $\implies$ $\overline{L} = \Sigma^+$

---

### $L_1 \cap L_2$

**Intersection** of two `language sets`, is the **All Elements** that are member of **First Set** and **Second Set**, indicates with $L_1 \cap L_2$

$$
\begin{aligned}
    & L_1 \cap L_2 = \{w : w \in L_1 \;AND\; w \in L_2\}
    \\
    \\
    \texttt{Example}:
    \\
    & L_1 = \{a, bb, ab\}
    \\
    & L_2 = \{ab, c\}
    \\
    \\
    & L_1 \cap L_2 = \{ab\}
\end{aligned}
$$

-   **Tip**: $L \cap \overline{L} = \emptyset$
-   **Tip**: $(L_1 \cap L_2)^* \subseteq L_1^* \cap L_2^*$
-   **Tip**: $L(M \cap N) \subseteq LM \cap LN$

---

### $L_1 \cup L_2$

**Union** of two `language sets`, is the **All Elements** that are member of **First Set** or **Second Set**, indicates with $L_1 \cup L_2$ or $L_1 + L_2$

$$
\begin{aligned}
    & L_1 \cup L_2 = \overline{\overline{L_1} \cap \overline{L_2}} = L_1 + L_2 = \{w : w \in L_1 \;OR\; w \in L_2\}
    \\
    \\
    \texttt{Example}:
    \\
    & L_1 = \{a, bb, ab\}
    \\
    & L_2 = \{ab, c\}
    \\
    \\
    & L_1 \cup L_2 = L_1 + L_2 = \{a, bb, ab, c\}
\end{aligned}
$$

-   **Tip**: $L \cup \overline{L} = \Sigma^*$
-   **Tip**: $L_1^* + L_2^* \subseteq (L_1 + L_2)^*$
-   **Tip**: $LM + LN = L(M + N)$

---

### $L_1 - L_2$

**Difference** of two `language sets`, is the **All Elements** that are member of **First Set** and not in **Second Set**, indicates with $L_1 - L_2$

$$
\begin{aligned}
    & L_1 - L_2 = L_1 \cap \overline{L_2} = \{w : w \in L_1 \;AND\; w \not\in L_2\}
    \\
    & L_2 - L_1 = L_2 \cap \overline{L_1} = \{w : w \in L_2 \;AND\; w \not\in L_1\}
    \\
    \\
    \texttt{Example}:
    \\
    & L_1 = \{a, bb, ab\}
    \\
    & L_2 = \{ab, c\}
    \\
    \\
    & L_1 - L_2 = \{a, bb\}
    \\
    & L_2 - L_1 = \{c\}
\end{aligned}
$$

---

### $L_1 \,\,/\,\, L_2$

**Quotient** of two `language sets`, is **Prefixes** or **Suffixes** of a language by **Removing** the second language strings from it, indicates with $L_1 \,\,/\,\, L_2$ and $L_1 \setminus\, L_2$

$$
\begin{aligned}
    & L_1 \,\,/\,\, L_2 = \{w : wx \in L_1, x \in L_2\}: \texttt{Prefixes of } L_1
    \\
    & L_1 \setminus\, L_2 = \{w : xw \in L_1, x \in L_2\}: \texttt{Suffixes of } L_1
    \\
    \\
    \texttt{Example}:
    \\
    & L_1 = \{baaba, bbbb, aabaa\}
    \\
    & L_2 = \{aba, aab, bb\}
    \\
    \\
    & L_1 \,\,/\,\, L_2 =
    \{
    (baaba / \left.\begin{cases}
        aba \\ aab \\ bb
    \end{cases}\right\})
    ,
    (bbbb / \left.\begin{cases}
        aba \\ aab \\ bb
    \end{cases}\right\})
    ,
    (aabaa / \left.\begin{cases}
        aba \\ aab \\ bb
    \end{cases}\right\})
    \}
    \\
    & L_1 \,\,/\,\, L_2 =
    \{
    \left.\begin{cases}
        ba \\ \times \\ \times
    \end{cases}\right\}
    ,
    \left.\begin{cases}
        \times \\ \times \\ bb
    \end{cases}\right\}
    ,
    \left.\begin{cases}
        \times \\ \times \\ \times
    \end{cases}\right\}
    \} = \{ba, bb\}
    \\
    \\
    & L_1 \setminus\, L_2 =
    \{
    (baaba \setminus \left.\begin{cases}
        aba \\ aab \\ bb
    \end{cases}\right\})
    ,
    (bbbb \setminus \left.\begin{cases}
        aba \\ aab \\ bb
    \end{cases}\right\})
    ,
    (aabaa \setminus \left.\begin{cases}
        aba \\ aab \\ bb
    \end{cases}\right\})
    \}
    \\
    & L_1 \setminus\, L_2 =
    \{
    \left.\begin{cases}
        \times \\ \times \\ \times
    \end{cases}\right\}
    ,
    \left.\begin{cases}
        \times \\ \times \\ bb
    \end{cases}\right\}
    ,
    \left.\begin{cases}
        \times \\ aa \\ \times
    \end{cases}\right\}
    \} = \{bb, aa\}
\end{aligned}
$$

-   **Tip**: $L_1 \,\,/\,\, L_2 \neq L_1 \setminus\, L_2$
-   **Tip**: $L_1 \,\,/\,\, L_2 = (L_1^R \setminus\, L_2^R)^R$
-   **Tip**: $L_1 \setminus\, L_2 = (L_1^R \,\,/\,\, L_2^R)^R$
-   **Tip**: $L_1 \,\,/\,\, L_2 \subseteq Prefixes(L_1)$
-   **Tip**: $L_1 \setminus\, L_2 \subseteq Suffixes(L_1)$
-   **Tip**:
    -   $$
        \begin{aligned}
            & \lambda \in L_2 \implies
            \begin{cases}
                L_1 \subseteq L_1 \,\,/\,\, L_2
                \\
                L_1 \subseteq L_1 \setminus\, L_2
            \end{cases}
        \end{aligned}
        $$

---

### $L_1 \times L_2$

**Product** or **Cross Product** or **Cartesian Product** of two `language sets`, is **Concatenation** of each element of **First Set** with each element of **Second Set**, indicates with $L_1 \times L_2$ or $L_1.L_2$

$$
\begin{aligned}
    & L_1.L_2 = \{u.v : u \in L_1, v \in L_2\}
    \\
    & L_2.L_1 = \{v.u : u \in L_1, v \in L_2\}
    \\
    \\
    \texttt{Example}:
    \\
    & L_1 = \{a, bb, ab\}
    \\
    & L_2 = \{aa, c\}
    \\
    \\
    & L_1.L_2 =
    \{
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        aa \\ c
    \end{cases}\right\}
    \} = \{aaa, ac, bbaa, bbc, abaa, abc\}
\end{aligned}
$$

-   **Tip**: $|L_1.L_2| \leq |L_1| \times |L_2|$: It will **Factorize Duplicated** strings
    -   $$
          \begin{aligned}
              & L_1 = \{a, b, ba\}
              \\
              & L_2 = \{a, aa\}
              \\
              \\
              & L_1.L_2 = \{aa, aaa, ba, baa, baa, baaa\}
              \\
              & L_1.L_2 = \{aa, aaa, ba, baa, baaa\}
              \\ \implies
              & |L_1.L_2| = 5 \lt 3*2
          \end{aligned}
        $$
-   **Tip**: $\{\lambda\}.L = L.\{\lambda\} = L$
-   **Tip**: $\emptyset.L = L.\emptyset = \emptyset$
-   **Tip**: $L_1.L_2 \neq L_2.L_1$

---

### $L^k$

**Exponent** of a `language set`, is **Producting** a **Set** with itself **k Times**, indicates with $L^k$

$$
\begin{aligned}
    & L^k = \underbrace{L.L.\dots.L}_{k} = \{u_1.u_2.\dots.u_k : u_1,u_2,\dots,u_k \in L\}
    \\
    \\
    \texttt{Example}:
    \\
    & L = \{a, bb, ab\}
    \\
    \\
    & L^0 = \{\lambda\}
    \\
    \\
    & L^1 = L =
    \{
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \} = \{a, bb, ab\}
    \\
    \\
    & L^2 = L.L =
    \{
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \} = \{aa, abb, aab, bba, bbbb, bbab, aba, abbb, abab\}
    \\
    \\
    & L^3 = L.L.L =
    \{
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \} = \{aaa, aabb, \dots, ababbb, ababab\}
    \\
    \\
    & L^k = \underbrace{L.L.\dots.L}_{\text{k times}} =
    \{
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \times
    \dots
    \times
    \left.\begin{cases}
        a \\ bb \\ ab
    \end{cases}\right\}
    \} = \{aa \dots a, \dots, abab \dots ab\}
    \\
    \\
    \texttt{Example}:
    \\
    & L = \{a^n.b^n , n \geq 0\}
    \\
    \\
    & L^2 =
    \{
    \left.\begin{cases}
        a^n.b^n, n \geq 0
    \end{cases}\right\}
    \times
    \left.\begin{cases}
        a^m.b^m, m \geq 0
    \end{cases}\right\}
    \} = \{a^n.b^n.a^m.b^m, n \geq 0, m \geq 0\}
\end{aligned}
$$

-   **Tip**: $|L^k| \leq k.|L|$
-   **Tip**: $L^0 = \{\lambda\}$
-   **Tip**: $L^n = L.L^{n-1} = L^{n-1}.L$

---

### $L^*$

**Kleene Star** of a `language set`, is the **Union** of $L^k$ for **k** from $0$ to $\infty$, indicates with $L^*$

$$
\begin{aligned}
    & L^* = \bigcup_{i=0}^{\infty} L^i = L^0 \cup L^1 \cup L^2 \cup \dots
    \\
    \\
    \texttt{Example}:
    \\
    & L = \{aa, ab, ba, bb\} = \Sigma^2
    \\ \implies
    & L^* = (\Sigma^2)^0 \cup (\Sigma^2)^1 \cup (\Sigma^2)^2 \cup \dots
    \\ \implies
    & L^* = \Sigma^0 \cup \Sigma^2 \cup \Sigma^4 \cup \dots
    \\ \implies
    & L^* = \{w : w \in \Sigma^* \;\&\; |w| \;mod\; 2 = 0\}
\end{aligned}
$$

-   **Tip**: If $L$ contains at least one **Non Empty String**, $L^*$ is **Not Null**
    -   $$
        \begin{aligned}
            & L = \emptyset \iff
            \left.\begin{cases}
                L^* = \{\lambda\}
                \\
                L^+ = \{\}
            \end{cases}\right\}
            \\
            & L = \{\lambda\} \iff
            \left.\begin{cases}
                L^* = \{\lambda\}
                \\
                L^+ = \{\lambda\}
            \end{cases}\right\}
        \end{aligned}
        $$
-   **Tip**: $L^* = (L^*)^* = (L^*)^+ = (L^+)^* = L^*.L^*$
-   **Tip**: $L_1 \subseteq L_2 \implies L_1^* \subseteq L_2^*$
-   **Tip**: $L_1^* \subseteq L_2^* \nRightarrow L_1 \subseteq L_2$
-   **Tip**: $(L_1.L_2)^* \neq L_1^*.L_2^*$
-   **Tip**: $(L_1.L_2)^*.L_1 = L_1.(L_2.L_1)^*$
-   **Tip**:
    -   $$
        \begin{aligned}
            & (L_1 + L_2)^* = (L_1^* + L_2^*)^* =
            \left.\begin{cases}
                (L_1 + L_2^*)^*
                \\
                \\
                (L_1^* + L_2)^*
            \end{cases}\right\} =
            \left.\begin{cases}
                (L_1^*.L_2^*)^*
                \\
                \\
                (L_2^*.L_1^*)^*
            \end{cases}\right\} =
            \left.\begin{cases}
                L_1^*.(L_2.L_1^*)^*
                \\
                \\
                (L_1^*.L_2)^*.L_1^*
            \end{cases}\right\}
        \end{aligned}
        $$

---

### $L^+$

**Kleene Plus** of a `language set`, is the **Union** of $L^k$ for **k** from $1$ to $\infty$, indicates with $L^+$

$$
\begin{aligned}
    & L^+ = \bigcup_{i=1}^{\infty} L^i = L^1 \cup L^2 \cup L^3 \cup \dots
    \\
    \\
    & L^+ = L^* - \{\lambda\}
\end{aligned}
$$

-   **Tip**: $\Sigma^+$ contains all **Possible Strings** with `non-zero size`
-   **Tip**: $L^+ = (L^+)^+ = L.L^* = L^*.L = L^*.L.L^*$
-   **Tip**: $L^+ + \lambda = L^*$

---

### $L^R$

**Reversal** of a `language set`, is another set contains **Reversal of Strings**, indicates with $L^R$

$$
\begin{aligned}
    & L^R = \{w^R : w \in L\}
    \\
    \\
    \texttt{Example}:
    \\
    & L_1 = \{ab, aaa, abc\}
    \\
    & L_1^R = \{ba, aaa, cba\}
    \\
    \\
    & L_2 = \{a^n.b^n : n \geq 0\}
    \\
    & L_2^R = \{b^n.a^n : n \geq 0\}
\end{aligned}
$$

-   **Tip**: $|L| = |L^R|$
-   **Tip**: $(L^*)^R = (L^R)^*$
-   **Tip**: $(L_1 + L_2)^R = L_1^R + L_2^R$
-   **Tip**: $(L_1.L_2)^R = L_2^R.L_1^R$

---

#### Palindrome

**Languages** that `equals` to their **Reverse**

-   For each **String** in language **Reverse String** `exists` in language

$$
\begin{aligned}
    & \text{Palindrome} \iff \forall u \in L : u^R \in L
    \\
    \\
    \text{Example}:
    \\
    & L = \{a, ab, ba\}
    \\ \implies
    & L = L^R
\end{aligned}
$$

-   **Tip**: Number of **Possible Palindromes** of size $n$ using an alphabet set $\Sigma$ is $|\Sigma|^{\lceil \frac{n}{2} \rceil}$
-   **Tip**: **Exponentiation** and **Reversal** of `one palindrome language` is also palindrome:
    -   $$
        \begin{aligned}
            &
            \left.\begin{cases}
                L: \text{Palindrome}
            \end{cases}\right\}
            \implies
            \left.\begin{cases}
                L.L: \text{Palindrome}
                \\
                L^n: \text{Palindrome}
                \\
                L^R: \text{Palindrome}
            \end{cases}\right\}
        \end{aligned}
        $$
-   **Tip**: **Union** and **Intersection** of `two palindrome languages` is also palindrome:
    -   $$
        \begin{aligned}
            &
            \left.\begin{cases}
                L_1: \text{Palindrome}
                \\
                L_2: \text{Palindrome}
            \end{cases}\right\}
            \implies
            \left.\begin{cases}
                L_1 \cup L_2: \text{Palindrome}
                \\
                L_1 \cap L_2: \text{Palindrome}
            \end{cases}\right\}
        \end{aligned}
        $$
-   **Tip**: Biggest **Palindrome Language** is $\Sigma^*$ contains **All Palindrome Strings** and **All Non-Palindrome Strings**:
    -   $$
        \begin{aligned}
            & \Sigma^* = \text{Palindrome Strings} \cup \text{Non-Palindrome Strings}
        \end{aligned}
        $$
-   **Tip**: Language containes **All String** of size **k** ($\Sigma^k$) is palindrome
    -   $$
        \begin{aligned}
            & L = \Sigma^k \implies L: \text{Palindrome}
            \\
            \\
            & \text{Example}:
            \\
            & L = \{aa, ab, ba, bb\}
            \\
            & L = L^R
        \end{aligned}
        $$
-   **Tip**: **Palindrome Language** may doesn't have any **Palindrome String**
    -   $$
        \begin{aligned}
            & \{aab, baa\} \implies L = L^R
        \end{aligned}
        $$

---

### $h(L)$

**Homomorph** or **Mapping** of a `language set`, is a mapping of strings by **h(x)** function, indicates with $h(L)$

$$
\begin{aligned}
    & h(L) = \{h(w) : w \in L\}
    \\
    & h^{-1}(L) = \{w : h(w) \in L\}
    \\
    \\
    & h(w) = h(a_1).h(a_2).\dots.h(a_n)
    \\
    \\
    \texttt{Example}:
    \\
    & L = \{a^{2n} b^{2m+1} : n,m \geq 1\}
    \\
    & h(a) = cc
    \\
    & h(b) = bd
    \\
    \\
    & h(L) = (cccc)^+.(bdbd)^+.bd
\end{aligned}
$$

-   **Tip**: $h(h^{-1}(L)) \subseteq L$
-   **Tip**: $h^{-1}(h(L)) = L$

---
