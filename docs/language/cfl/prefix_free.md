# Prefix Free Language (PFL)

Is a **Formal Language** that can defined by a **Prefix Free Grammar**

-   **Prefix-Free Language**: Infinite language that `no string` is the `prefix` of `other strings` in language

---

## Detection

There are **Three Ways** of detecting a language is **Prefix Free** or not:

-   **Language**
-   **Grammar**
-   **Automata**

---

### Language

If the language has **Expression** `without prefix strings` it is prefix-free

-   **Prefix Strings**: Two or more strings which one of them is prefix of another

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \text{No Prefix} \implies \checkmark
    \\
    & \{w \in \Sigma^* : n_a(w) = n_b(w)\} \implies \{\underbrace{ab, abab}_{\text{Prefix}}, \dots\} \implies \times
\end{aligned}
$$

---

### Grammar

If the language has **Prefix Free Grammar** it is prefix-free

---

### Automata

If the language has **Push-Down Automata** with `empty-stack acceptor` it is prefix-free

-   **Empty-Stack Acceptor**: A PDA which accept strings when `halt's` which `empty stack`

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \text{DPDA-ES} \implies \checkmark
    \\
    & \{w \in \Sigma^* : n_a(w) = n_b(w)\} \implies \times
\end{aligned}
$$

---
