# Linear Language (LL)

Is a **Formal Language** that can defined by a **Linear Grammar**

---

## Detection

There are **Three Ways** of detecting a language is **Linear** or not:

-   **Language**
-   **Grammar**
-   **Automata**

---

### Language

If the language has **Expression** with `only one set` of `two dependent variables` it is linear

-   **Dependent Variable**: Two or more expression variables which are related to $n$

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \{\underbrace{a^n b^n}_{n} \underbrace{a^m b^m}_{m}\} \implies \times
    \\
    & \{a^n b^n c^n\} \implies \{\underbrace{a^n b^n c^n}_{n}\} \implies \times
    \\
    & \{a^n b^n\} \implies \{\underbrace{a^n b^n}_{n}\} \implies \checkmark
\end{aligned}
$$

---

### Grammar

If the language has **Linear Grammar** it is linear

---

### Automata

If the language has **Push-Down Automata** with `one-turn stack` it is linear

-   **One Turn Stack**: A stack that goes up and down only once

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \nearrow \searrow \nearrow \searrow \implies \times
    \\
    & \{a^n b^n c^n\} \implies \text{Not Context-Free} \implies \times
    \\
    & \{a^n b^n\} \implies \nearrow \searrow \implies \checkmark
\end{aligned}
$$

---

**Example 1**: Decide which of these languages is **Linear**:

$$
\begin{aligned}
    & L_1 = \{a^n b^{5n+1} : n \geq 0\}
    \\ \implies
    & \{\underbrace{a^n b^{5n+1}}_{n}\} \implies \checkmark
    \\
    \\
    & L_2 = \{a^n b^n a^m b^{2m} : n,m \geq 0\}
    \\ \implies
    & \{\underbrace{a^n b^n}_{n} \underbrace{a^m b^{2m}}_{m}\} \implies \times
    \\
    \\
    & L_3 = \{a^n b^n c^m : n,m \geq 0\}
    \\ \implies
    & \{\underbrace{a^n b^n}_{n} c^m\} \implies \checkmark
    \\
    \\
    & L_4 = \{a^n b^n c^m : n \lt m\}
    \\ \implies
    & \{\underbrace{a^n b^n c^{\gt n}}_{n}\} \implies \times
    \\
    \\
    & L_5 = \{w\#w^R : w \in \Sigma^*\}
    \\ \implies
    & \nearrow \searrow \implies \checkmark
    \\
    \\
    & L_6 = \{w \in \Sigma^* : n_a(w) = n_b(w)\}
    \\ \implies
    & \nearrow \searrow \nearrow \searrow \dots \nearrow \searrow \implies \times
    \\
    \\
    & L_7 = \{a^n b^n\} \cup \{a^n d^n\}
    \\ \implies
    & \text{Linear} \cup \text{Linear} = \text{Linear} \implies \checkmark
    \\
    \\
    & L_8 = \{a^n b^n\} \cup \{a^n b^{2n}\}
    \\ \implies
    & \text{Linear} \cup \text{Linear} = \text{Linear} \implies \checkmark
\end{aligned}
$$

---

## Pumping Lemma

Is a lemma used to proof that a **Language** is **Not Linear**, we **Cannot** use it for proofing a language is **Linear**:

-   **Find** a **Dependent Cycle** in PDA
-   **Find** a **String** that can generate by PDA cycle, but not in language
-   $$
    \begin{aligned}
        & \exists w \in L(M) = u.v^i.x.y^i.z : w \not\in L \implies L: \text{Non-Linear}
    \end{aligned}
    $$

$$
\begin{aligned}
    & \exists c \geq 0 : \forall w \in L, |w| \geq c
    \\ \implies
    & w = uvxyz, |vy| \geq 1, |uvyz| \leq c
    \\
    \\ \implies
    & \forall i \geq 0 : u.v^i.x.y^i.z \in L
\end{aligned}
$$

---

## Closure Operations

There are many operations on **Linear Languages** that the output is also **Linear**

---

### $L_1 \cap R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Linear}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies L_1 \cap R_2: \text{Linear}
\end{aligned}
$$

---

### $L_1 \cup L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Linear}
        \\
        L_2: \text{Linear}
    \end{cases}\right\} \implies L_1 \cup L_2: \text{Linear}
\end{aligned}
$$

---

### $L_1 - R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Linear}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 - R_2: \text{Linear}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Linear}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times R_2: \text{Linear}
        \\
        R_2 \times L_1: \text{Linear}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L^R$

$$
\begin{aligned}
    & L: \text{Linear} \implies L^R: \text{Linear}
\end{aligned}
$$

---

### $h(L)$

$$
\begin{aligned}
    & L: \text{Linear} \implies h(L): \text{Linear}
\end{aligned}
$$

---
