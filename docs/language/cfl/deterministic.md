# Deterministic Context Free Language (DCFL)

Is a **Formal Language** that can defined by a **Deterministic Context Free Grammar**

---

## Detection

There are **Three Ways** of detecting a language is **Deterministic Context Free** or not:

-   **Language**
-   **Grammar**
-   **Automata**

---

### Language

If the language has **Infinite Expression** `without decision point` for `next expression` it is deterministic context-free

-   **Infinite Decision Point**: After `accepting infinite` number of symbols we should `decide` how much expression we should accept

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies
    a^n \mapsto b^n \mapsto a^m \mapsto b^m
    \implies \checkmark
    \\
    & \{a^n b^n\} \cup \{a^n b^{2n}\} \implies
    a^n \mapsto
    \begin{cases}
        b^n: \text{Pop 00}
        \\
        b^{2n}: \text{Pop 0}
    \end{cases}
    \implies \times
\end{aligned}
$$

---

### Grammar

If the language has **Deterministic Context Free Grammar** it is deterministic context-free

---

### Automata

If the language has **Push-Down Automata** with `final-state acceptor` it is deterministic context-free

-   **Final-State Acceptor**: A PDA which accept strings when `halt's` in `final state`

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \text{DPDA-FS} \implies \checkmark
    \\
    & \{w \in \Sigma^* : n_a(w) = n_b(w)\} \implies \text{DPDA-FS} \implies \checkmark
    \\
    & \{a^n b^n\} \cup \{a^n b^{2n}\} \implies \times
\end{aligned}
$$

---

**Example 1**: Decide which of these languages is **Deterministic Context-Free**:

$$
\begin{aligned}
    & L_1 = \{a^n b^n : n \geq 0\}
    \\ \implies
    & a^n \mapsto b^n \implies \checkmark
    \\
    \\
    & L_2 = \{a^n b^n a^m b^m : n,m \geq 0\}
    \\ \implies
    &
    \left.\begin{cases}
        \{a^n b^n\}
        \\
        \{a^n b^n\}
    \end{cases}\right\} \implies a^n \mapsto
    \begin{cases}
        b^n: \text{Pop 0}
        \\
        b^n: \text{Pop 0}
    \end{cases} \implies \checkmark
    \\
    \\
    & L_3 = \{a^n b^n a^m b^{2m} : n,m \geq 0\}
    \\ \implies
    &
    \left.\begin{cases}
        \{a^n b^n\}
        \\
        \{a^n b^{2n}\}
    \end{cases}\right\} \implies a^n \mapsto
    \begin{cases}
        b^n: \text{Pop 00}
        \\
        b^{2n}: \text{Pop 0}
    \end{cases} \implies \times
    \\
    \\
    & L_4 = \{a^n b^n a^m b^{2m} : n \geq 1, m \geq 0\}
    \\ \implies
    & a^n \mapsto b^n \mapsto a^m \mapsto b^{2m} \implies \checkmark
    \\
    \\
    & L_5 = \{a^n b^n c^m : n,m \geq 0\}
    \\ \implies
    & a^n \mapsto b^n \mapsto c^m \implies \checkmark
    \\
    \\
    & L_6 = \{a^n b^n c^m : n \leq m\}
    \\ \implies
    & \text{Non Context-Free} \implies \times
    \\
    \\
    & L_7 = \{w\#w^R : w \in \Sigma^*\}
    \\ \implies
    & \text{DPDA} \implies \checkmark
    \\
    \\
    & L_8 = \{w \in \Sigma^* : n_a(w) = n_b(w)\}
    \\ \implies
    & \text{DPDA} \implies \checkmark
    \\
    \\
    & L_9 = \{a^n b^n\} \cup \{a^n d^n\}
    \\ \implies
    &
    \left.\begin{cases}
        \{a^n b^n\}
        \\
        \{a^n d^n\}
    \end{cases}\right\} \implies a^n \mapsto
    \begin{cases}
        b^n: \text{Pop 0}
        \\
        d^n: \text{Pop 0}
    \end{cases} \implies \checkmark
    \\
    \\
    & L_{10} = \{a^n b^n\} \cup \{a^n b^{2n}\}
    \\ \implies
    &
    \left.\begin{cases}
        \{a^n b^n\}
        \\
        \{a^n b^{2n}\}
    \end{cases}\right\} \implies a^n \mapsto
    \begin{cases}
        b^n: \text{Pop 00}
        \\
        b^{2n}: \text{Pop 0}
    \end{cases} \implies \times
\end{aligned}
$$

---

## Closure Operations

There are many operations on **Deterministic Context Free Languages** that the output is also **Deterministic Context Free**

---

### $\overline{L}$

$$
\begin{aligned}
    & L: \text{Deterministic CFL} \iff \overline{L}: \text{Deterministic CFL}
\end{aligned}
$$

---

### $L_1 \cap R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Deterministic CFL}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies L_1 \cap R_2: \text{Deterministic CFL}
\end{aligned}
$$

---

### $L_1 \cup R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Deterministic CFL}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies L_1 \cup R_2: \text{Deterministic CFL}
\end{aligned}
$$

---

### $L_1 - R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Deterministic CFL}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 - R_2: \text{Deterministic CFL}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Deterministic CFL}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times R_2: \text{Deterministic CFL}
        \\
        R_2 \times L_1: \text{Deterministic CFL}
    \end{cases}\right\}
\end{aligned}
$$

---
