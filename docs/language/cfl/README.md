# Context Free Language (CFL)

Is a **Formal Language** that can defined by a **Context Free Grammar**

There are four categories of **Context Free Languages**:

-   **Linear**:
    -   Can describe by **Linear Grammar**
    -   Can accept by **One-Turn PDA**
-   **Prefix Free**:
    -   Can describe by **Prefix Free Grammar**
    -   Can accept by **DPDA-ES**
-   **Deterministic Context Free**:
    -   Can describe by **Deterministic Context Free Grammar**
    -   Can accept by **DPDA-FS**
-   **Context Free**:
    -   Can describe by **Context Free Grammar**
    -   Can accept by **PDA**

![Categories](../../assets/cfl_categories.png)

-   **Tip**: Most of the `programming languages` are `context-free language`

---

## CFL => Automata

Converting a **Context Free Language** to **Push-Down Automata** is very simple:

-   **Find** `context-free expression` of language
-   **Use** regular languages `rules table` to convert `regular expressions`
-   **Use** `stack` for checking `relation` between two `regular expressions`

---

**Example 1**: Find **Push-Down Automata** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{a^n b^n : n \geq 0\}
\end{aligned}
$$

![Automata Example 1](../../assets/cfl_to_automata_example_1.png)

---

**Example 2**: Find **Push-Down Automata** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{a^n b^{4n} : n \geq 0\}
\end{aligned}
$$

There are three ways of solving this problem:

-   **Method 1**:
    -   **Push** `0` when `a`
    -   **Pop** `0` when `bbbb`
-   **Method 2**:
    -   **Push** `0000` when `a`
    -   **Pop** `0` when `b`
-   **Method 3**:
    -   **Push** `0` when `a`
    -   **Increase** to `4` when `b` then `pop`

![Automata Example 2](../../assets/cfl_to_automata_example_2.png)

---

**Example 3**: Find **Push-Down Automata** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{w \in \Sigma^* : n_a(w) = n_b(w)\}
\end{aligned}
$$

There is a way of solving this problem:

-   **Method 1**:
    -   **Push** `0` when top is `0` and input is `a`
    -   **Push** `1` when top is `1` and input is `b`
    -   **Pop** `1` when top is `1` and input is `a`
    -   **Pop** `0` when top is `0` and input is `b`

![Automata Example 3](../../assets/cfl_to_automata_example_3.png)

---

**Example 4**: Find **Push-Down Automata** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{a^n b^m c^{n+m} : n,m \geq 0\}
\end{aligned}
$$

![Automata Example 4](../../assets/cfl_to_automata_example_4.png)

---

## CFL => Grammar

There is a **Bottom-Up** approach to find a **Grammar** of a **Context Free Language**:

-   **Find** the `context-free expression` of language
-   **Split** language `expressions`
-   **Write** `grammar` for each part
-   **Merge** grammars together

There are some useful partitions and their grammars:

-   $ww^R$ $\implies$ $S \longrightarrow aSa \;|\; bSb$
-   $a^n b^n$ $\implies$ $S \longrightarrow aSb$

---

**Example 1**: Find **Context Free Grammar** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{a^n b^m c^k : n = m+k\}
    \\
    \\ \implies
    & L = \{a^{m+k} b^m c^k : m,k \geq 0\}
    \\
    & L = \{a^k a^m b^m c^k : m,k \geq 0\}
    \\
    \\ \implies
    & L_1 = a^m b^m \implies
    \left.\begin{cases}
        A \longrightarrow aAb \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_2 = a^k c^k \implies
    \left.\begin{cases}
        B \longrightarrow aBc \;|\; \lambda
    \end{cases}\right\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow A \;|\; B
        \\
        A \longrightarrow aAb \;|\; \lambda
        \\
        B \longrightarrow aBc \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 2**: Find **Context Free Grammar** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{a^n b^m c^{2k+1} a^{2m+1} b^{2n} : n,m,k \geq 0\}
    \\
    \\ \implies
    & L_1 = a^n.b^{2n} \implies
    \left.\begin{cases}
        S \longrightarrow aSbb \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_2 = b^m.a^{2m+1} \implies
    \left.\begin{cases}
        A \longrightarrow bAaa \;|\; a
    \end{cases}\right\}
    \\
    & L_3 = c^{2k+1} \implies
    \left.\begin{cases}
        B \longrightarrow ccB \;|\; c
    \end{cases}\right\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow aSbb \;|\; A
        \\
        A \longrightarrow bAaa \;|\; Ba
        \\
        B \longrightarrow ccB \;|\; c
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 3**: Find **Context Free Grammar** for this **Palindrome Language**:

$$
\begin{aligned}
    & L_1 = \{ww^R : w \in \Sigma^*\}
    \\
    & L_2 = \{w(a+b)w^R : w \in \Sigma^*\}
    \\
    & L_3 = \{w : w \in \Sigma^* , w = w^R\}
    \\
    \\ \implies
    & L_1 = w (\lambda) w^R \implies
    \left.\begin{cases}
        S \longrightarrow aSa \;|\; bSb \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_2 = w (a + b) w^R \implies
    \left.\begin{cases}
        S \longrightarrow aSa \;|\; bSb \;|\; a \;|\; b
    \end{cases}\right\}
    \\
    & L_3 = w (a + b + \lambda) w^R \implies
    \left.\begin{cases}
        S \longrightarrow aSa \;|\; bSb \;|\; a \;|\; b \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 4**: Find **Context Free Grammar** for this **Non-Palindrome Language**:

-   **Concept**: Before the end, at least, one time **Non-Equal** symbols at `left`, `right` and **Change State**

$$
\begin{aligned}
    & L_1 = \{w_1w_2 : w_1,w_2 \in \Sigma^* , w_1 \neq w_2^R\}
    \\
    & L_2 = \{w : w \in \Sigma^* , w \neq w^R\}
    \\
    \\ \implies
    & L_1 = w_1 (\lambda) w_2 \implies
    \left.\begin{cases}
        S \longrightarrow aSa \;|\; bSb \;|\; aAb \;|\; bAa
        \\
        A \longrightarrow aAa \;|\; bAb \;|\; aAb \;|\; bAa \;|\; \lambda
    \end{cases}\right\}
    \\
    & L_2 = w_1 (a + b + \lambda) w_2 \implies
    \left.\begin{cases}
        S \longrightarrow aSa \;|\; bSb \;|\; aAb \;|\; bAa
        \\
        A \longrightarrow aAa \;|\; bAb \;|\; aAb \;|\; bAa \;|\; a \;|\; b \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 5**: Find **Context Free Grammar** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{a^n b^m : n \neq m\}
    \\
    \\ \implies
    & L = \{a^n b^m : n \lt m\} \cup \{a^n b^m : n \gt m\}
    \\
    & L = \{a^n b^{k+1} b^n : n,k \geq 0\} \cup \{a^m a^{k+1} b^m : m,k \geq 0\}
    \\
    \\ \implies
    & L_1 = \{a^n b^{k+1} b^n : n,k \geq 0\} \implies
    \left.\begin{cases}
        A_1 \longrightarrow a A_1 b \;|\; A_2
        \\
        A_2 \longrightarrow bA_2 \;|\; b
    \end{cases}\right\}
    \\
    & L_2 = \{a^m a^{k+1} b^m : m,k \geq 0\} \implies
    \left.\begin{cases}
        B_1 \longrightarrow a B_1 b \;|\; B_2
        \\
        B_2 \longrightarrow aB_2 \;|\; a
    \end{cases}\right\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow A_1 \;|\; B_1
        \\
        A_1 \longrightarrow a A_1 b \;|\; A_2
        \\
        A_2 \longrightarrow bA_2 \;|\; b
        \\
        B_1 \longrightarrow a B_1 b \;|\; B_2
        \\
        B_2 \longrightarrow aB_2 \;|\; a
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 6**: Find **Context Free Grammar** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{w \in \{a,b\}^* : n_a(w) = n_b(w)\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow aSb \;|\; bSa \;|\; SS \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 7**: Find **Context Free Grammar** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{w \in \{a,b\}^* : n_a(w) = n_b(w) + 2\}
    \\
    \\ \implies
    & L = \{n_a(w) = n_b(w)\}.a.\{n_a(w) = n_b(w)\}.a.\{n_a(w) = n_b(w)\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow XaXaX
        \\
        X \longrightarrow aXb \;|\; bXa \;|\; XX \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

**Example 8**: Find **Context Free Grammar** for this **Context Free Language**:

$$
\begin{aligned}
    & L = \{w \in \{a,b\}^* : n_a(w) = n_b(w) , \forall u = Prefix(w) : n_a(u) \geq n_b(u)\}
    \\
    \\ \implies
    & G:
    \left.\begin{cases}
        S \longrightarrow aSb \;|\; SS \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---
