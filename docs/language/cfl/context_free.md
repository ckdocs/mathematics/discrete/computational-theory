# Context Free Language (CFL)

Is a **Formal Language** that can defined by a **Context Free Grammar**

-   **Tip**: $\texttt{Context Free} = \texttt{Non-Deterministic Context Free}$
-   **Tip**: If the language was **Single Symbol**:
    -   $\text{Context-Free}$ $\implies$ $\text{Regular}$
    -   $\text{Non-Regular}$ $\implies$ $\text{Non-Context-Free}$
    -   $a^n: \text{Context-Free}$ $\implies$ $a^n: \text{Regular}$
    -   $a^p: \text{Non-Regular}$ $\implies$ $a^p: \text{Non-Context-Free}$

---

## Detection

There are **Three Ways** of detecting a language is **Context Free** or not:

-   **Language**
-   **Grammar**
-   **Automata**

---

### Language

If the language has **Expression** with `any set` of `two dependent variables` it is context-free

-   **Dependent Variable**: Two or more expression variables which are related to $n$

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \{\underbrace{a^n b^n}_{n} \underbrace{a^m b^m}_{m}\} \implies \checkmark
    \\
    & \{a^n b^n c^n\} \implies \{\underbrace{a^n b^n c^n}_{n}\} \implies \times
    \\
    & \{a^n b^n\} \implies \{\underbrace{a^n b^n}_{n}\} \implies \checkmark
\end{aligned}
$$

---

### Grammar

If the language has **Context Free Grammar** it is context-free

---

### Automata

If the language has **Non-Deterministic Push-Down Automata** it is context-free

$$
\begin{aligned}
    & \{a^n b^n a^m b^m\} \implies \text{PDA} \implies \checkmark
    \\
    & \{a^n b^n c^n\} \implies \times
    \\
    & \{a^n b^n\} \implies \text{PDA} \implies \checkmark
\end{aligned}
$$

---

**Example 1**: Decide which of these languages is **Context-Free**:

$$
\begin{aligned}
    & L_1 = \{a^n b^n c^k : n \geq 0, n \geq k\}
    \\ \implies
    & \{\underbrace{a^n b^n c^{\leq n}}_{n}\} \implies \times
    \\
    \\
    & L_2 = \{a^n b^m c^k : n \leq m \leq k\}
    \\ \implies
    & \{\underbrace{a^{\leq m} b^m c^{\geq m}}_{m}\} \implies \times
    \\
    \\
    & L_3 = \{a^n b^m c^k : n = m \;OR\; m \neq k\}
    \\ \implies
    & \{\underbrace{a^n b^n}_{n} c^k\} \cup \{a^n \underbrace{b^m c^{\neq m}}_{m}\} \implies \checkmark
    \\
    \\
    & L_4 = \{w w^R : w \in \Sigma^*\}
    \\ \implies
    & \text{PDA} \implies \checkmark
    \\
    \\
    & L_5 = \{w w : w \in \Sigma^*\}
    \\ \implies
    & \text{No PDA} \implies \times
    \\
    \\
    & L_6 = \{a^{n^2} : n \geq 0\}
    \\ \implies
    & \text{Compute n^2} = \text{No PDA} \implies \times
    \\
    \\
    & L_7 = \{a^p : p: \text{Prime}\}
    \\ \implies
    & \text{Compute primes} = \text{No PDA} \implies \times
\end{aligned}
$$

---

## Pumping Lemma

Is a lemma used to proof that a **Language** is **Not Context-Free**, we **Cannot** use it for proofing a language is **Context-Free**:

-   **Find** a **Dependent Cycle** in PDA
-   **Find** a **String** that can generate by PDA cycle, but not in language
-   $$
    \begin{aligned}
        & \exists w \in L(M) = u.v^i.x.y^i.z : w \not\in L \implies L: \text{Non-Context-Free}
    \end{aligned}
    $$

$$
\begin{aligned}
    & \exists c \geq 0 : \forall w \in L, |w| \geq c
    \\ \implies
    & w = uvxyz, |vy| \geq 1, |vxy| \leq c
    \\
    \\ \implies
    & \forall i \geq 0 : u.v^i.x.y^i.z \in L
\end{aligned}
$$

---

## Closure Operations

There are many operations on **Context-Free Languages** that the output is also **Context-Free**

---

### $L_1 \cap R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Free}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies L_1 \cap R_2: \text{Context-Free}
\end{aligned}
$$

---

### $L_1 \cup L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Free}
        \\
        L_2: \text{Context-Free}
    \end{cases}\right\} \implies L_1 \cup L_2: \text{Context-Free}
\end{aligned}
$$

---

### $L_1 - R_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Free}
        \\
        R_2: \text{Regular}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 - R_2: \text{Context-Free}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L_1 \times L_2$

$$
\begin{aligned}
    & \left.\begin{cases}
        L_1: \text{Context-Free}
        \\
        L_2: \text{Context-Free}
    \end{cases}\right\} \implies
    \left.\begin{cases}
        L_1 \times L_2: \text{Context-Free}
        \\
        L_2 \times L_1: \text{Context-Free}
    \end{cases}\right\}
\end{aligned}
$$

---

### $L^*$

$$
\begin{aligned}
    & L: \text{Context-Free} \implies L^*: \text{Context-Free}
\end{aligned}
$$

---

### $L^+$

$$
\begin{aligned}
    & L: \text{Context-Free} \implies L^+: \text{Context-Free}
\end{aligned}
$$

---

### $L^R$

$$
\begin{aligned}
    & L: \text{Context-Free} \implies L^R: \text{Context-Free}
\end{aligned}
$$

---

### $h(L)$

$$
\begin{aligned}
    & L: \text{Context-Free} \implies h(L): \text{Context-Free}
\end{aligned}
$$

---
