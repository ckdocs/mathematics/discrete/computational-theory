# Context Sensitive Grammar (CSG)

Is a **Grammar** for describing **Context Sensitive Languages**

There are two categories of **Context Sensitive Grammars**:

-   **Deterministic Context Sensitive Grammar (DCSG)**: Equivalent to `DLBA`
-   **Non-Deterministic Context Sensitive Grammar (CSG)**: Equivalent to `NLBA`

We don't know that the power of these types is **Equal or Not**

---

## CSG => Language

There is a **Top-Down** approach to find the **Language of a Grammar** (`L(G)`):

<!-- TODO -->

---

## CSG => Automata

Converting a **Context Sensitive Grammar** to **LBA** is very simple:

<!-- TODO -->

---
