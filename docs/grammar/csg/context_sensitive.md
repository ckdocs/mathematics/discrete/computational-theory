# Context Sensitive Grammar (CSG)

Is a grammar for describing **Context Sensitive Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{\alpha A \beta \rightarrow \alpha \gamma \beta\}
    \\
    & A \in V
    \\
    & \alpha, \beta \in (V \cup T)^*
    \\
    & \gamma \in (V \cup T)^+
\end{aligned}
$$

---
