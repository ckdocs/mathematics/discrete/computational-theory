# Unrestricted Grammar (UG)

Is a grammar for describing **Recursively Enumerable Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{\alpha \rightarrow \beta\}
    \\
    & \alpha \in (V \cup T)^+
    \\
    & \beta \in (V \cup T)^*
\end{aligned}
$$

---
