# Unrestricted Grammar (UG)

Is a **Grammar** for describing **Recursively Enumerable Languages**

There is one category of **Unrestricted Grammars**:

-   **Unrestricted Grammar (UG)**: Equivalent to `TM`

---

## UG => Language

There is a **Top-Down** approach to find the **Language of a Grammar** (`L(G)`):

<!-- TODO -->

---

## UG => Automata

Converting a **Unrestricted Grammar** to **TM** is very simple:

<!-- TODO -->

---
