# Grammar

Is a way to **Describe** a `formal language`, indicates with $G$:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & V: \text{Variables (Non-Terminal) set}
    \\
    & T: \text{Terminals set}
    \\
    & S: \text{Start symbol}
    \\
    & P: \text{Production rules}
    \\
    \\
    \text{Example}:
    \\
    & G = (V,T,S,P)
    \\
    \\
    & V = \{S, A, B\}
    \\
    & T = \{a, b\}
    \\
    & S = S
    \\
    & P =
    \left.\begin{cases}
        S \longrightarrow AB
        \\
        A \longrightarrow a
        \\
        B \longrightarrow b
    \end{cases}\right\}
\end{aligned}
$$

-   **Tip**: Variables are often **Capital Case**: $A, B, C$
-   **Tip**: Terminals are often **Small Case**: $a, b, c$
-   **Tip**: A production rule is in form $\alpha \longrightarrow \beta$ which:
    -   **Left Side**: $\alpha \in (V \cup T)^*$
    -   **Right Side**: $\beta \in (V \cup T)^*$
-   **Tip**: In `informal` definition of grammars, we only define **Production Rules**
    -   **Left Side** of the **First Grammar** is `start symbol`

---

## Concepts

There are many important concepts in **Grammar**:

-   **Production**: Grammar `rules` for `derivation`
-   **Sentence**: Each state of `terminals`, `variables`
-   **Derivation**: Movement from `one sentence` to `another` by `production rules`
-   **Determinism**: Number of `possible` derivations from a `sentential`
-   **Language**: All strings that can `generate` by grammar `derivations`

---

### Dependency Graph

Is a graph that `visualizes` grammar **Variables Dependency**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow aXbc \;|\; abc
        \\
        Xb \longrightarrow bX
        \\
        Xc \longrightarrow Ybcc
        \\
        bY \longrightarrow Yb
        \\
        aY \longrightarrow aaX \;|\; aa
    \end{cases}\right\}
\end{aligned}
$$

![Dependency Graph](../assets/grammar_dependency.png)

-   **Tip**: Using `dependency graph` we can find the **Sentence** generator `loops`

---

### Complexity

The summation of **RHS** length of all **Production Rules** `plus one` called complexity

$$
\begin{aligned}
    & \forall \{A \longrightarrow \alpha\} \in P : \sum(|\alpha|+1)
    \\
    \\
    \text{Example}:
    \\
    & S \longrightarrow \underbrace{AB}_{2+1}
    \\
    & A \longrightarrow \underbrace{aA}_{2+1} \;|\; \underbrace{bB}_{2+1}
    \\
    & B \longrightarrow \underbrace{Eg}_{2+1} \;|\; \underbrace{Be}_{2+1}
    \\
    & E \longrightarrow \underbrace{mE}_{2+1} \;|\; \underbrace{\lambda}_{0+1}
    \\
    \\ \implies
    & \text{Complexity}: 3 + 3 + 3 + 3 + 3 + 3 + 1 = 19
\end{aligned}
$$

-   **Tip**: By removing **Useless productions**, we can **Reduce** the grammar `complexity`

---

### Production

All grammar **Rules** used for **Derivation** of `sententials`

There are one way of describing **Production Rules** of a **Grammar**:

-   **Production Rule**: A syntax of describing replacement rules of **LHS** by **RHS**
    -   **Left Hand Side (LHS)**: Left side of production rules `replaced by RHS`
    -   **Right Hand Side (RHS)**: Right side of production rules `replaces to LHS`
    -   $$
        \begin{aligned}
            & \underbrace{\alpha A \beta}_{LHS} \longrightarrow \underbrace{\alpha^{\prime} B \beta^{\prime}}_{RHS}
        \end{aligned}
        $$

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow aAb
        \\
        aA \longrightarrow aaAb
        \\
        A \longrightarrow \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

### Sentential

Each combination of **Terminals** and **Variables** that can `derivate` using `production rules` of grammar called a **Sentential**

There are two important types of strings:

-   **Sentential Form**: A string contains **Variables** or **Terminals**
-   **Sentence**: A string contains only **Terminals**
-   $$
    \begin{aligned}
        & SF(G) = \{w \in (V \cup T)^* : S \overset{*}{\implies} w\}
        \\
        & L(G) = \{w \in T^* : S \overset{*}{\implies} w\}
    \end{aligned}
    $$

---

**Example**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow aAb
        \\
        aA \longrightarrow aaAb
        \\
        A \longrightarrow \lambda
    \end{cases}\right\}
    \\
    \\
    & \underbrace{S \implies aAb \implies aaAbb \implies aaaAbbb}_{\text{Sentential Form}} \implies \underbrace{aaabbb}_{\text{Sentence}}
\end{aligned}
$$

---

### Derivation

Moving from one **Sentential** to another by applying a **Production Rule** (Replace `LHS` by `RHS`), called a **Derivation**

$$
\begin{aligned}
    &
    \left.\begin{cases}
        \text{LHS}
    \end{cases}\right\}
    \underbrace{\implies}_{\text{Production Rule}}
    \left.\begin{cases}
        \text{RHS}
    \end{cases}\right\}
\end{aligned}
$$

There are two ways of describing **Derivation Path** to generate a **String** from **Initial Variable**:

-   **Derivation Tree**: Is a tree that shows the **Production Rules** that used
-   **Derivation Sequence**: Is a sequence of **Production Rules** that used
    -   There are three **Derivation Symbols**:
        -   $\overset{}{\implies}$: **One step** derivation
        -   $\overset{+}{\implies}$: **1 to k steps** derivation
        -   $\overset{*}{\implies}$: **0 to k steps** derivation

---

**Example**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow aAb
        \\
        aA \longrightarrow aaAb
        \\
        A \longrightarrow \lambda
    \end{cases}\right\}
    \\
    \\
    & S \underbrace{\implies}_{\text{Derivation}} aAb \underbrace{\implies}_{\text{Derivation}} aaAbb \underbrace{\implies}_{\text{Derivation}} aaaAbbb \underbrace{\implies}_{\text{Derivation}} aaabbb
\end{aligned}
$$

---

#### Left-Most Derivation

**Left-Most Derivation** or **LMD** is a special derivation, which **First** we apply **Left-Most Variables** in each step

---

**Example**: Find the **Left-Most Derivation Tree**:

$$
\begin{aligned}
    & \text{Grammar}: X \longrightarrow X+X \;|\; X*X \;|\; X \;|\; a
    \\
    & \text{Sentence}: a+a*a
\end{aligned}
$$

![Left-Most Derivation](../assets/grammar_derivation_leftmost.jpg)

$$
\begin{aligned}
    & X \implies X+X \implies a+X \implies a + X*X \implies a+a*X \implies a+a*a
\end{aligned}
$$

---

#### Right-Most Derivation

**Right-Most Derivation** or **RMD** is a special derivation, which **First** we apply **Right-Most Variables** in each step

---

**Example**: Find the **Right-Most Derivation Tree**:

$$
\begin{aligned}
    & \text{Grammar}: X \longrightarrow X+X \;|\; X*X \;|\; X \;|\; a
    \\
    & \text{Sentence}: a+a*a
\end{aligned}
$$

![Right-Most Derivation](../assets/grammar_derivation_rightmost.jpg)

$$
\begin{aligned}
    & X \implies X*X \implies X*a \implies X+X*a \implies X+a*a \implies a+a*a
\end{aligned}
$$

---

### Determinism

There are two categories of **Grammars**:

-   **Deterministic**: In each **Sentential** grammar has only **One Possible Production**
-   **Non-Deterministic**: In each **Sentential** grammar has **Many Possible Productions**

$$
\begin{aligned}
    & \text{Deterministic Grammar}:
    \begin{cases}
        A \longrightarrow aB \;|\; bC
        \\
        X \longrightarrow Y
    \end{cases}
    \\
    \\
    & \text{Non-Deterministic Grammar}:
    \begin{cases}
        A \longrightarrow aB \;|\; aC
        \\
        X \longrightarrow Y \;|\; Z
    \end{cases}
\end{aligned}
$$

---

### Language

**Language of a grammar** is all the strings that **Grammar** can **Generate**:

$$
\begin{aligned}
    & L(G) = \{w \in T^* : S \overset{*}{\implies} w\}
\end{aligned}
$$

-   **Tip**: There are **Infinite** number of **Grammars** for each **Language**
    -   If we can find at least **One Grammar** for a language, we can find $\infty$ number of grammars
-   **Tip**: There is only **One Language** for each **Grammar**
-   **Tip**: The **Minimized Grammar** for each language is **Unique**

![Language](../assets/grammar_language.png)

---

#### Equivalent

Two grammars are **Equivalent** if and only if their **Languages** are equal:

$$
\begin{aligned}
    & \left.\begin{cases}
        L(G_1) \subseteq L(G_2)
        \\
        L(G_2) \subseteq L(G_1)
    \end{cases}\right\} \iff L(G_1) = L(G_2) \iff G_1 = G_2
    \\
    \\
    \text{Example}:
    \\
    & G_1:
    \left.\begin{cases}
        S \longrightarrow aaaS \;|\;B
        \\
        B \longrightarrow bbbB \;|\; \lambda
    \end{cases}\right\}
    \\
    & G_2:
    \left.\begin{cases}
        S \longrightarrow Saaa \;|\;B
        \\
        B \longrightarrow Bbbb \;|\; \lambda
    \end{cases}\right\}
    \\
    & G_3:
    \left.\begin{cases}
        S \longrightarrow aaaS \;|\;B
        \\
        B \longrightarrow Bbbb \;|\; \lambda
    \end{cases}\right\}
    \\
    \\
    & L(G_1) = L(G_2) = L(G_3) \implies G_1 = G_2 = G_3
\end{aligned}
$$

---

## Categories

The **Power** of a grammar depends on the **Number of Variables** at **Left-side, Right-side**, there are four categories of grammars:

-   **Regular** (RG):
    -   Grammar for describing **Regular** languages
    -   **Production Rules**:
        -   **Left-side**: One variable
        -   **Right-side**: One variable
    -   $$
        \begin{aligned}
            & A \longrightarrow Ax \;|\; x
            \\
            & B \longrightarrow xB \;|\; x
            \\
            & C \longrightarrow xCy \;|\; xy
            \\
            \\
            & A,B,C \in V
            \\
            & x,y \in T^*
        \end{aligned}
        $$
-   **Context-Free** (CFG):
    -   Grammar for describing **Context-free** languages
    -   **Production Rules**:
        -   **Left-side**: One variable
        -   **Right-side**: Many variables
    -   $$
        \begin{aligned}
            & A \longrightarrow \alpha
            \\
            \\
            & A \in V
            \\
            & \alpha \in (V \cup T)^*
        \end{aligned}
        $$
-   **Context-Sensitive** (CSG):
    -   Grammar for describing **Context-sensitive** languages
    -   **Production Rules**:
        -   **Left-side**: Many variables
        -   **Right-side**: Many variables
    -   $$
        \begin{aligned}
            & \alpha \longrightarrow \beta
            \\
            \\
            & \alpha, \beta \in (V \cup T)^*
            \\
            & |\alpha| \leq |\beta|
        \end{aligned}
        $$
-   **Unrestricted** (UG):
    -   Grammar for describing **Recursively enumerable** languages
    -   **Production Rules**:
        -   **Left-side**: Many variables
        -   **Right-side**: Many variables
    -   $$
        \begin{aligned}
            & \alpha \longrightarrow \beta
            \\
            \\
            & \alpha \in (V \cup T)^+
            \\
            & \beta \in (V \cup T)^*
        \end{aligned}
        $$

---
