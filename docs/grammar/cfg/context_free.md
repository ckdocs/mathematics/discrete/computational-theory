# Context Free Grammar (CFG)

Is a grammar for describing **Context Free Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{X \rightarrow \alpha\}
    \\
    & X \in V
    \\
    & \alpha \in (V \cup T)^*
\end{aligned}
$$

---

## Ambiguity

A **Context Free Grammar** called **Ambiguous** when it can generate `more than one` **Left-Most Derivation** or `more than one` **Right-Most Derivation** for a `string`

-   **Tip**: The problem of ambiguity is that **Operations Precedence** may change:
    -   $$
        \begin{aligned}
            & 2 + 3 * 5 \implies
            \begin{cases}
                2 + (3 * 5) = 17 \checkmark
                \\
                (2 + 3) * 5 = 25 \times
            \end{cases}
        \end{aligned}
        $$
-   **Tip**: Resolving the ambiguity means, **Changing** the grammar in order to **Operators** have only **One Precedence**

---

**Example**: Is this grammar **Ambigous**?

$$
\begin{aligned}
    & \text{Grammar}: X \longrightarrow X+X \;|\; X*X \;|\; X \;|\; a
    \\
    & \text{Sentence}: a+a*a
\end{aligned}
$$

![Ambiguity](../../assets/grammar_ambiguity.png)

$$
\begin{aligned}
    & X \implies X+X \implies a+X \implies a+X*X \implies a+a*X \implies a+a*a
    \\
    \\
    & X \implies X*X \implies X+X*X \implies a+X*X \implies a+a*X \implies a+a*a
\end{aligned}
$$

---
