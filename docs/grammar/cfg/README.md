# Context Free Grammar (CFG)

Is a **Grammar** for describing **Context Free Languages**

There are four categories of **Context Free Grammars**:

-   **Linear Grammar (LG)**: Equivalent to `one-turn NPDA`
-   **Prefix Free Grammar (PFG)**: Equivalent to `DPDA-ES`
-   **Deterministic Context Free Grammar (DCFG)**: Equivalent to `DPDA-FS`
-   **Non-Deterministic Context Free Grammar (CFG)**: Equivalent to `NPDA`

The power of these types are **Not Equal** and we cannot turn the weaker into the stronger

---

## Simplification

Removing the unnecessary **Production Rules** and **Symbols** for **Derivation** from CFG grammar in three steps **In Order**:

-   Remove **Null Productions**
-   Remove **Unit Productions**
-   Remove **Useless Symbols**
-   Add **Root Null Production** if language has $\lambda$
    -   **Tip**: In **Standard Simplification** is step should not be done
    -   $$
        \begin{aligned}
            & S^{\prime} \longrightarrow S \;|\; \lambda
        \end{aligned}
        $$

There are some tips in **CFG Simplification**:

-   **Tip**: Removing `null productions` can **Generate** some **Unit Productions**
-   **Tip**: Removing `null productions` can **Generate** some **Useless Productions**
-   **Tip**: Removing `unit productions` can **Generate** some **Useless Productions**
-   **Tip**: So the `order` of **Simplification** steps is very important

![Simplification](../../assets/cfg_simplification.png)

---

### Null Productions

Removing all the **Null Productions** in the form $A \longrightarrow \lambda$

-   **Nullable Variable**: A grammar variable which `can derivate` $\lambda$ after `some steps`
-   $$
    \begin{aligned}
        & A \underbrace{\overset{*}{\implies}}_{\text{Nullable}} \lambda
    \end{aligned}
    $$

Removing the **Null Productions** is very simple:

-   **Find** all `nullable variables`
-   **Add** all `nullable variables permutations` in **RHS** of `productions`
-   **Remove** all `null` productions
-   **Remove** the `same` productions

$$
\begin{aligned}
    & S \longrightarrow AxB
    \\
    & A,B: \text{Nullable variable}
    \\
    \\ \implies
    & S \longrightarrow x \;|\; Ax \;|\; xB \;|\; AxB
\end{aligned}
$$

-   **Tip**: Removing `null productions` can **Increase** the **Derivation Speed**
-   **Tip**: Removing `null productions` can **Increase** the **Grammar Complexity**

---

**Example**: Remove all **Null Productions** in this **Grammar**:

$$
\begin{aligned}
    & S \longrightarrow ABgC
    \\
    & A \longrightarrow Aa \;|\; BE
    \\
    & B \longrightarrow bB \;|\; \lambda
    \\
    & C \longrightarrow Cd \;|\; x \;|\; \lambda
    \\
    & E \longrightarrow eE \;|\; Ee \;|\; \lambda
\end{aligned}
$$

$$
\begin{aligned}
    \\ \underset{\text{Find nullables}}{\implies}
    & \left.\begin{cases}
        S \longrightarrow ABgC
        \\
        (A) \longrightarrow Aa \;|\; BE
        \\
        (B) \longrightarrow bB \;|\; \lambda
        \\
        (C) \longrightarrow Cd \;|\; x \;|\; \lambda
        \\
        (E) \longrightarrow eE \;|\; Ee \;|\; \lambda
    \end{cases}\right\}
    \\ \underset{\text{Add permutations}}{\implies}
    & \left.\begin{cases}
        S \longrightarrow (ABgC \;|\; BgC \;|\; AgC \;|\; ABg \;|\; gC \;|\; Bg \;|\; Ag \;|\; g)
        \\
        (A) \longrightarrow (Aa \;|\; a) \;|\; (BE \;|\; B \;|\; E \;|\; \lambda)
        \\
        (B) \longrightarrow (bB \;|\; b) \;|\; \lambda
        \\
        (C) \longrightarrow (Cd \;|\; d) \;|\; x \;|\; \lambda
        \\
        (E) \longrightarrow (eE \;|\; e) \;|\; (Ee \;|\; e) \;|\; \lambda
    \end{cases}\right\}
    \\ \underset{\text{Remove nulls}}{\implies}
    & \left.\begin{cases}
        S \longrightarrow (ABgC \;|\; BgC \;|\; AgC \;|\; ABg \;|\; gC \;|\; Bg \;|\; Ag \;|\; g)
        \\
        (A) \longrightarrow (Aa \;|\; a) \;|\; (BE \;|\; B \;|\; E)
        \\
        (B) \longrightarrow (bB \;|\; b)
        \\
        (C) \longrightarrow (Cd \;|\; d) \;|\; x
        \\
        (E) \longrightarrow (eE \;|\; e) \;|\; (Ee \;|\; e)
    \end{cases}\right\}
\end{aligned}
$$

---

### Unit Productions

Removing all the **Unit Productions** in the form $A \longrightarrow B$

-   **Unit Production**: A production rule which have `only one variable` in **RHS**
-   $$
    \begin{aligned}
        & A \longrightarrow B
        \\
        & A,B \in V
    \end{aligned}
    $$

Removing the **Unit Productions** is very simple:

-   **Add** all `unit variables productions` in **RHS** of `productions`
-   **Remove** all `unit` productions
-   **Remove** the `same` productions

$$
\begin{aligned}
    & S \longrightarrow A
    \\
    & A \longrightarrow x \;|\; y \;|\; z
    \\
    \\ \implies
    & S \longrightarrow x \;|\; y \;|\; z
\end{aligned}
$$

-   **Tip**: Removing `unit productions` can **Increase** the **Derivation Speed**
-   **Tip**: Removing `unit productions` can **Increase** the **Grammar Complexity**

---

**Example**: Remove all **Unit Productions** in this **Grammar**:

$$
\begin{aligned}
    & S \longrightarrow AB \;|\; C \;|\; g
    \\
    & A \longrightarrow aA \;|\; B \;|\; e
    \\
    & B \longrightarrow Aa \;|\; C \;|\; e \;|\; m
    \\
    & C \longrightarrow Cb \;|\; BA \;|\; A \;|\; \lambda
\end{aligned}
$$

$$
\begin{aligned}
    \\ \implies
    & \left.\begin{cases}
        & S \longrightarrow AB \;|\; g \;|\; Rules(C)
        \\
        & A \longrightarrow aA \;|\; e \;|\; Rules(B)
        \\
        & B \longrightarrow Aa \;|\; e \;|\; m \;|\; Rules(C)
        \\
        & C \longrightarrow Cb \;|\; BA \;|\; \lambda \;|\; Rules(A)
    \end{cases}\right\}
    \\ \implies
    & \left.\begin{cases}
        & S \longrightarrow AB \;|\; g \;|\; Rules(C)
        \\
        & A \longrightarrow BA \;|\; Cb \;|\; aA \;|\; Aa \;|\; e \;|\; m \;|\; \lambda
        \\
        & B \longrightarrow BA \;|\; Cb \;|\; aA \;|\; Aa \;|\; e \;|\; m \;|\; \lambda
        \\
        & C \longrightarrow BA \;|\; Cb \;|\; aA \;|\; Aa \;|\; e \;|\; m \;|\; \lambda
    \end{cases}\right\}
    \\ \implies
    & \left.\begin{cases}
        & S \longrightarrow AB \;|\; g \;|\; BA \;|\; Cb \;|\; aA \;|\; Aa \;|\; e \;|\; m \;|\; \lambda
        \\
        & A \longrightarrow BA \;|\; Cb \;|\; aA \;|\; Aa \;|\; e \;|\; m \;|\; \lambda
        \\
        & B \longrightarrow BA \;|\; Cb \;|\; aA \;|\; Aa \;|\; e \;|\; m \;|\; \lambda
        \\
        & C \longrightarrow BA \;|\; Cb \;|\; aA \;|\; Aa \;|\; e \;|\; m \;|\; \lambda
    \end{cases}\right\}
\end{aligned}
$$

---

### Useless Productions

Removing all the **Useless Productions** which has at least one **Useless Variable** at their **RHS**:

-   **Reachable Variable**: A grammar variable which `cannot access` from `starting variable`
-   **Generating Variable**: A grammar variable which `cannot generate` any `terminal`
-   **Useless Variable**: A grammar variable which is **Non-Reachable** or **Non-Generating**
-   $$
    \begin{aligned}
        & S \underbrace{\overset{*}{\implies}}_{\text{Reachable}} \alpha A \beta \underbrace{\overset{*}{\implies}}_{\text{Generating}} w
    \end{aligned}
    $$

Removing the **Useless Productions** is very simple:

-   **Remove** all **Non-Generating** variables (`bottom-up`)
    -   **Write** all `dependent` variables
    -   **Remove** `circular` dependencies
    -   **Solve** `bottom-up`
-   **Remove** all **Non-Reachable** variables (`top-down`)
    -   **Find** `dependency graph`
    -   **Remove** `unaccessable` variables
-   **Remove** all `productions` with **Non-Exists** variables

The **Order** of removing **Non-Generating** and **Non-Reachable** is important

---

**Example**: Remove all **Useless Productions** in this **Grammar**:

$$
\begin{aligned}
    & S \longrightarrow AB \;|\; AdCD
    \\
    & A \longrightarrow aA \;|\; bB
    \\
    & B \longrightarrow Eg \;|\; Be
    \\
    & E \longrightarrow mE \;|\; \lambda
    \\
    & D \longrightarrow Dn \;|\; \lambda
    \\
    & C \longrightarrow yC \;|\; Cx
\end{aligned}
$$

![Simplification Useless Production Example](../../assets/cfg_simplification_useless_example.png)

---

## Normal Forms

Converting any **Simplified Context Free Grammar** to a **Normal Form**, there are two common normal forms:

-   **Chomsky Normal Form (CNF)**
-   **Greibach Normal Form (GNF)**

There are some tips about **Normal Forms**:

-   **Tip**: We can convert any **CFG** without $\lambda$ to a **CNF** or **GNF**
-   **Tip**: If a **CFL** have $\lambda$, it doesn't have **Chomsky Normal Form**
-   **Tip**: The **CNF** and **GNF** of a **CFG** is **Not Unique**

---

### Chomsky Normal Form (CNF)

A **Simplified CFG** with production rules in this form:

$$
\begin{aligned}
    & A \longrightarrow BC \;|\; a
    \\
    \\
    & B,C \in V
    \\
    & a \in T
\end{aligned}
$$

Converting to **CNF** is very simple:

-   **Replace** each `terminal` with `unit production` $X \longrightarrow a$
-   **Break** each `RHS` into slices with length `less than 2`

There are some tips about **CNF**:

-   **Tip**: Number of `derivation steps` of a string using **CNF** grammar is $2.|w| - 1$
-   **Tip**: We can parse a string using **CYK** algorithm in $O(n^3)$
-   **Tip**: The **CNF** of a grammar is **Not Unique**

---

**Example**: Convert this **Grammar** to equivalent **CNF**:

$$
\begin{aligned}
    & S \longrightarrow aSa \;|\; bSb \;|\; aa \;|\; bb
    \\
    \\ \overset{\text{Replace terminals}}{\implies}
    & \begin{cases}
        S \longrightarrow ASA \;|\; BSB \;|\; AA \;|\; BB
        \\
        A \longrightarrow a
        \\
        B \longrightarrow b
    \end{cases}
    \\
    \\ \overset{\text{Split productions}}{\implies}
    & \begin{cases}
        S \longrightarrow AS_1 \;|\; BS_2 \;|\; AA \;|\; BB
        \\
        S_1 \longrightarrow SA
        \\
        S_2 \longrightarrow SB
        \\
        A \longrightarrow a
        \\
        B \longrightarrow b
    \end{cases}
\end{aligned}
$$

---

### Greibach Normal Form (GNF)

A **Simplified CFG** with production rules in this form:

$$
\begin{aligned}
    & A \longrightarrow aX
    \\
    \\
    & X \in V^*
    \\
    & a \in T
\end{aligned}
$$

Converting to **GNF** is very simple:

-   **Replace** each `terminal` with `unit production` $X \longrightarrow a$
-   **Replace** each `first non-terminal` of `RHS` with `possible terminals` (**Remove Left Recursive**)
    -   $$
        \begin{aligned}
            & X \longrightarrow
            \begin{cases}
                X \alpha_1 \;|\; X \alpha_2 \;|\; \dots \;|\; X \alpha_n
                \\
                \beta_1 \;|\; \beta_2 \;|\; \dots \;|\; \beta_m
            \end{cases}
            \\
            \\ \implies
            & X \longrightarrow
            \begin{cases}
                \beta_1 X^{\prime} \;|\; \beta_2 X^{\prime} \;|\; \dots \;|\; \beta_m X^{\prime}
                \\
                \beta_1 \;|\; \beta_2 \;|\; \dots \;|\; \beta_m
            \end{cases}
            \\
            & X^{\prime} \longrightarrow
            \begin{cases}
                \alpha_1 X^{\prime} \;|\; \alpha_2 X^{\prime} \;|\; \dots \;|\; \alpha_n X^{\prime}
                \\
                \alpha_1 \;|\; \alpha_2 \;|\; \dots \;|\; \alpha_n
            \end{cases}
        \end{aligned}
        $$

There are some tips about **GNF**:

-   **Tip**: Number of `derivation steps` of a string using **GNF** grammar is $|w|$
-   **Tip**: The **GNF** of a grammar is **Not Unique**
-   **Tip**: Each **Simple Grammar** is a **GNF**

---

**Example**: Convert this **Grammar** to equivalent **GNF**:

$$
\begin{aligned}
    & S \longrightarrow ABd
    \\
    & A \longrightarrow aAg \;|\; eb
    \\
    & B \longrightarrow Be \;|\; y
    \\
    \\ \implies
    & \begin{cases}
        S \longrightarrow ABX_1
        \\
        A \longrightarrow aAX_2 \;|\; eX_3
        \\
        B \longrightarrow yB^{\prime} \;|\; y
        \\
        B^{\prime} \longrightarrow e \;|\; eB^{\prime}
        \\
        X_1 \longrightarrow d
        \\
        X_2 \longrightarrow g
        \\
        X_3 \longrightarrow b
    \end{cases}
\end{aligned}
$$

---

## CFG => Language

There is a **Top-Down** approach to find the **Language of a Grammar** (`L(G)`):

-   **Simplify** all `recursive grammars`
-   **Create** a `derivation tree`
-   **Grow** tree and find `sentences`
-   **Find** a relation between `sentences`

---

**Example 1**: Find **Language** for this **Context Free Grammar**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow AB
        \\
        A \longrightarrow aaA \;|\; a
        \\
        B \longrightarrow bbB \;|\; b
    \end{cases}\right\}
    \\
    \\ \implies
    & L(G) =
    \left.\begin{cases}
        S \longrightarrow AB
        \\
        A \longrightarrow a^{2n+1}
        \\
        B \longrightarrow b^{2m+1}
    \end{cases}\right\}
    \\
    & L(G) = \{ab, aaab, aaabbb, \dots\}
    \\
    & L(G) = \{a^{2n+1} b^{2m+1} : n,m \geq 0\}
\end{aligned}
$$

---

## CFG => Automata

Converting a **Context Free Grammar** to **PDA** is very simple:

-   **Convert** the `CFG` into `GNF` form
-   **Create** state $q_0$ as `initial state`
-   **Create** state $q_f$ as `final state`
-   **Create** state $q$ as `middle state`
-   **Foreach** production rule $A \longrightarrow aX$
    -   **Create** a `loop edge` on $q$ as $\delta(q_0, a, A) = (q, X)$

![Grammar to Automata](../../assets/cfg_to_automata.png)

-   **Tip**: For each CFG grammar, exist a **PDA** with only `3 states`, but we can reduce the number of states into `2`:
    -   ![Minimized Grammar to Automata](../../assets/cfg_to_automata_minimized.png)

---

**Example 1**: Find **PDA** for this **Context Free Grammar**:

$$
\begin{aligned}
    & S \mapsto aAB \;|\; b
    \\
    & A \mapsto dAA \;|\; g
    \\
    & B \mapsto bB \;|\; d
\end{aligned}
$$

![Automata Example 1](../../assets/cfg_to_automata_example_1.png)

---
