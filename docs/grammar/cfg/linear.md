# Linear Grammar (LG)

Is a grammar for describing **Linear Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{X \rightarrow aYb \;|\; \lambda\}
    \\
    & X,Y \in V
    \\
    & a,b \in T^*
\end{aligned}
$$

-   **Tip**: The **RHS** of production rules, contains only one `variable`
-   **Example**: The grammar bellow `is` **LG**:
    -   $$
        \begin{aligned}
            & S \longrightarrow aSb \;|\; \lambda
        \end{aligned}
        $$

---
