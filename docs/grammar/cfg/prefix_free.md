# Prefix Free Grammar (PFG)

Is a grammar for describing **Prefix Free Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{X \rightarrow aY \;|\; \lambda\}
    \\
    & X \in V
    \\
    & Y \in V^*
    \\
    & a \in T
    \\
    \\
    & \text{(X, a) occurs only one time}
\end{aligned}
$$

-   **Example**: The grammar bellow `is` **PFG**:
    -   $$
        \begin{aligned}
            & S \longrightarrow aAB \;|\; bAA
            \\
            & A \longrightarrow aAB \;|\; bBB
            \\
            & B \longrightarrow bB \;|\; d
        \end{aligned}
        $$
-   **Example**: The grammar bellow `is not` **PFG**:
    -   $$
        \begin{aligned}
            & S \longrightarrow aAB \;|\; bA
            \\
            & A \longrightarrow \underbrace{a}_{*}AB \;|\; \underbrace{a}_{*}BB
            \\
            & B \longrightarrow a \;|\; dB
        \end{aligned}
        $$

---
