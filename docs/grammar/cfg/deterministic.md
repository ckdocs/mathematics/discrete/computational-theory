# Deterministic Context Free Grammar (DCFG)

Is a grammar for describing **Deterministic Context Free Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{X \rightarrow \alpha\}
    \\
    & X \in V
    \\
    & \alpha \in (V \cup T)^*
\end{aligned}
$$

-   **Tip**: This category of grammars used in **LR**, **SLR**, etc `parse algorithms` in `compilers`

---
