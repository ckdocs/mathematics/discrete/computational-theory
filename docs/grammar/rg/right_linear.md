# Right Linear Grammar (RLG)

Is a grammar for describing **Regular Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{X \rightarrow aY \;|\; a \;|\; \lambda\}
    \\
    & X,Y \in V
    \\
    & a \in T^*
\end{aligned}
$$

-   **Tip**: In **RHS** of production rules, `variable` is at `right-side`

---

## Right-Linear => Left-Linear

There is a way to convert a **Right-Linear** grammar to **Left-Linear** grammar

-   **Draw** the `GTG` of `right-linear` grammar
-   **Swap** the `final states` and `initial state`
-   **Reverse** the `edges` and their `labels`
-   **Find** `right-linear` grammar of new `GTG`
-   **Reverse** new grammar `rules`

---

**Example 1**: Find **Left-Linear** grammar of this grammar:

$$
\begin{aligned}
    & S \mapsto abA
    \\
    & A \mapsto bbB
    \\
    & B \mapsto abaA \;|\; aa
\end{aligned}
$$

![Right to Left Example 1](../../assets/rg_right_to_left_example_1.png)

$$
\begin{aligned}
    & C \mapsto aaaB
    \\
    & B \mapsto bbA
    \\
    & A \mapsto abaB \;|\; baS
    \\
    & S \mapsto \lambda
    \\
    \\ \implies
    & C \mapsto Baaa
    \\
    & B \mapsto Abb
    \\
    & A \mapsto Baba \;|\; Sab
    \\
    & S \mapsto \lambda
\end{aligned}
$$

---
