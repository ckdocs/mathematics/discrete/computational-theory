# Left Linear Grammar (LLG)

Is a grammar for describing **Regular Languages** in the form of:

$$
\begin{aligned}
    & G = (V,T,S,P)
    \\
    \\
    & P: \{X \rightarrow Ya \;|\; a \;|\; \lambda\}
    \\
    & X,Y \in V
    \\
    & a \in T^*
\end{aligned}
$$

-   **Tip**: In **RHS** of production rules, `variable` is at `left-side`

---

## Left-Linear => Right-Linear

There is a way to convert a **Left-Linear** grammar to **Right-Linear** grammar

---
