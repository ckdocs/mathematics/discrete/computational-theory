# Regular Grammar (RG)

Is a **Grammar** for describing **Regular Languages**

There are two categories of **Regular Grammars**:

-   **Left Linear Grammar (LLG)**: In **RHS** of production rules, `variable` is at `left-side`
-   **Right Linear Grammar (RLG)**: In **RHS** of production rules, `variable` is at `right-side`

The power of these types are **Equal** and we can convert them together

---

## RG => Language

There is a **Top-Down** approach to find the **Language of a Grammar** (`L(G)`):

-   **Simplify** all `recursive grammars`
-   **Create** a `derivation tree`
-   **Grow** tree and find `sentences`
-   **Find** a relation between `sentences`

Another way of finding **Language** of **Grammar** is to convert **Grammar** to a **DFA** and find **GTG** of **DFA**

---

**Example 1**: Find **Language** for this **Regular Grammar**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow AB
        \\
        A \longrightarrow aaA \;|\; a
        \\
        B \longrightarrow bbB \;|\; b
    \end{cases}\right\}
    \\
    \\ \implies
    & L(G) =
    \left.\begin{cases}
        S \longrightarrow AB
        \\
        A \longrightarrow a^{2n+1}
        \\
        B \longrightarrow b^{2m+1}
    \end{cases}\right\}
    \\
    & L(G) = \{ab, aaab, aaabbb, \dots\}
    \\
    & L(G) = \{a^{2n+1} b^{2m+1} : n,m \geq 0\}
\end{aligned}
$$

---

**Example 2**: Find **Language** for this **Regular Grammar**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow aSbb \;|\; A
        \\
        A \longrightarrow bAccc \;|\; bccc
    \end{cases}\right\}
    \\
    \\ \implies
    & L(G) =
    \left.\begin{cases}
        S \longrightarrow a^n A b^{2n}
        \\
        A \longrightarrow b^m c^{3m}
    \end{cases}\right\}
    \\
    & L(G) = \{a^n b^m c^{3m} d^{2n} : n \geq 0, m \geq 1\}
    \\
    & L(G) = \{a^n b^{m+1} c^{3m+1} d^{2n} : n,m \geq 0\}
\end{aligned}
$$

---

**Example 3**: Find **Language** for this **Regular Grammar**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow aaaS \;|\; aB \;|\; aaB
        \\
        B \longrightarrow bbbbbB \;|\; b \;|\; bb \;|\; bbb \;|\; bbbb
    \end{cases}\right\}
    \\
    \\ \implies
    & L(G) =
    \left.\begin{cases}
        S \longrightarrow a^{3n}
        \begin{cases}
            aB
            \\
            aaB
        \end{cases}
        \\
        B \longrightarrow b^{5m}
        \begin{cases}
            b
            \\
            bb
            \\
            bbb
            \\
            bbbb
        \end{cases}
    \end{cases}\right\}
    \\
    & L(G) =
    \left.\begin{cases}
        S \longrightarrow
        \begin{cases}
            a^{3n+1}
            \begin{cases}
                b^{5m+1}
                \\
                b^{5m+2}
                \\
                b^{5m+3}
                \\
                b^{5m+4}
            \end{cases}
            \\
            a^{3n+2}
            \begin{cases}
                b^{5m+1}
                \\
                b^{5m+2}
                \\
                b^{5m+3}
                \\
                b^{5m+4}
            \end{cases}
        \end{cases}
    \end{cases}\right\}
    \\
    & L(G) = \{a^n b^m : n \;mod\; 3 \neq 0, m \;mod\; 5 \neq 0\}
\end{aligned}
$$

---

**Example 4**: Find **Language** for this **Regular Grammar**:

$$
\begin{aligned}
    & G:
    \left.\begin{cases}
        S \longrightarrow aXbc \;|\; abc
        \\
        Xb \longrightarrow bX
        \\
        Xc \longrightarrow Ybcc
        \\
        bY \longrightarrow Yb
        \\
        aY \longrightarrow aaX \;|\; aa
    \end{cases}\right\}
    \\
    \\ \implies
    & L(G) = \{abc, aabbcc, aaabbbccc, \dots\}
    \\
    & L(G) = \{a^n b^n c^n : n \geq 1\}
\end{aligned}
$$

![Grammar to Language 4](../../assets/grammar_grammar_to_language_4.png)

---

## RG => Automata

Converting a **Right Linear Grammar** to **NFA** is very simple:

-   **Convert** each variable to a **State**
-   **Add** one **Final State** named **F**
-   **Foreach** rule like $A \mapsto aB$
    -   Add edge from **A** to **B** by $a$
-   **Foreach** rule like $A \mapsto B$
    -   Add edge from **A** to **B** by $\lambda$
-   **Foreach** rule like $A \mapsto a$
    -   Add edge from **A** to **F** by $a$
-   **Foreach** rule like $A \mapsto \lambda$
    -   Add edge from **A** to **F** by $\lambda$

---

**Example 1**: Find **NFA** for this **Regular Grammar**:

$$
\begin{aligned}
    & S \mapsto a \;|\; aA \;|\; bB \;|\; \lambda
    \\
    & A \mapsto aA \;|\; aS
    \\
    & B \mapsto cS \;|\; \lambda
\end{aligned}
$$

![Automata Example 1](../../assets/rg_to_automata_example_1.png)

---
