# String

Any **Sequence** of `alphabet symbols`, indicates with $S$ with three properties:

1. **Finite**
    - It has a finite size
    - $|S| = k$
2. **Can Empty**
    - It can be empty string
    - $S = \lambda,\; |S| = 0$
3. **Sequence**
    - The order of symbols is important
    - $abdd \neq addb$

$$
\begin{aligned}
    & S = abcddee \in \Sigma^*
\end{aligned}
$$

-   **Tip**: Strings are **Elements** of $\Sigma^*$
-   **Tip**: Strings often displayed with **u, v, w**

---

## Concepts

There are many **Concepts** in **Strings**

---

### Empty

There is an **Empty String** with size `0`, indicates with $\lambda$ or $\epsilon$

$$
\begin{aligned}
    & S = \lambda = \epsilon
    \\
    & |S| = 0
\end{aligned}
$$

-   **Tip**: Empty string is **Prefix** and **Suffix** of all string
    -   $$
        \begin{aligned}
            & S_1 = \lambda \implies \lambda
            \\
            & S_2 = a \implies \lambda a \lambda
            \\
            & S_3 = abcx \implies \lambda abcx \lambda
        \end{aligned}
        $$
-   **Tip**: Empty string is **Infix** of all symbols of strings
    -   $$
        \begin{aligned}
            & S = abcx \implies \lambda a \lambda b \lambda c \lambda x \lambda
        \end{aligned}
        $$

---

## Operations

There are many **Operations** available for **Strings**

---

### Length

**Length** or **Size** of a `string sequence`, is the **Number of Symbols** in it, indicates with $|S|$

$$
\begin{aligned}
    & S = aaabbababaabaa
    \\
    \\
    & |S| = n(S) = 14
    \\
    & |S|_a = n_a(S) = 9
    \\
    & |S|_b = n_b(S) = 5
\end{aligned}
$$

-   **Tip**: Sum of `alphabets size` in a string is `string size`
    -   $|S| = |S|_0 + |S|_1 + \dots + |S|_k$
    -   $$
        \begin{aligned}
            & S = 0011102002210
            \\
            & |S|_0 = 6
            \\
            & |S|_1 = 4
            \\
            & |S|_2 = 3
            \\
            \\
            & |S| = |S|_0 + |S|_1 + |S|_2 = 13
        \end{aligned}
        $$

---

### Substring

Is a **Contiguous Sequence** of `characters` within a string, it's a **Prefix** or **Infix** or **Suffix**

$$
\begin{aligned}
    & abc \in aaabcda
    \\
    & aaa \in aaabcda
\end{aligned}
$$

![Substring](./assets/string_substring.png)

-   **Tip**: Empty string ($\lambda$) is **Substring** of all strings
-   **Tip**: Number of **Possible Substrings** of a string is summation of `all sizes`
    -   $$
        \begin{aligned}
            & S = abbbc\dots aa
            \\
            \\
            & \texttt{Size 0 substrings}: 1 \implies \lambda
            \\
            & \texttt{Size 1 substrings}: |S|
            \\
            & \texttt{Size 2 substrings}: |S| - 1
            \\
            & \texttt{Size 3 substrings}: |S| - 2
            \\
            & \vdots
            \\
            & \texttt{Size |S| substrings}: 1
            \\
            \\ \implies
            & \text{Substrings} = 1 + (1 + 2 + 3 + \dots + |S|)
            \\
            & \text{Substrings} = 1 + \frac{|S|.(|S| + 1)}{2}
        \end{aligned}
        $$

---

### Prefix, Suffix

-   **Prefix**: All **Substrings** of a string that `starts from 1`
-   **Suffix**: All **Substrings** of a string that `ends at |S|`

$$
\begin{aligned}
    & S = aaabcda = \lambda aaabcda \lambda
    \\
    \\
    & aa \underset{\texttt{Prefix}}{\in} S
    \\
    & \lambda \underset{\texttt{Prefix}}{\in} S
    \\
    & da \underset{\texttt{Suffix}}{\in} S
    \\
    & \lambda \underset{\texttt{Suffix}}{\in} S
\end{aligned}
$$

![Prefix Suffix](./assets/string_prefix_suffix.png)

-   **Tip**: Number of **Possible Prefixes and Suffixes** of a string is `size of string` plus `1` (**Empty String**)
    -   $$
        \begin{aligned}
            & S = abbbc\dots aa
            \\
            \\
            & \text{Prefixes}: |S| + 1
            \\
            & \text{Suffixes}: |S| + 1
        \end{aligned}
        $$

---

### Concatenate

**Concatenation** of two `strings`, is the **Joining Strings End-to-End**, indicates with $S_1.S_2$

$$
\begin{aligned}
    & S_1 = abc
    \\
    & S_2 = xy
    \\
    \\
    & S_1.S_2 = \underbrace{abc}_{S_1} \underbrace{xy}_{S_2}
    \\
    & S_2.S_1 = \underbrace{xy}_{S_2} \underbrace{abc}_{S_1}
\end{aligned}
$$

-   **Tip**: $u.v \neq v.u$
-   **Tip**: $u.(v.w) = (u.v).w$
-   **Tip**: $u.\lambda = \lambda.u = u$
-   **Tip**: $u.\emptyset = \emptyset.u = \emptyset$
-   **Tip**: $|u.v| = |u| + |v|$

---

### Exponent

Is **Concatenating** a **String** with itself `k times`

$$
\begin{aligned}
    & S = abc
    \\
    \\
    & S^4 = S.S.S.S = \underbrace{abc}_{S}\underbrace{abc}_{S}\underbrace{abc}_{S}\underbrace{abc}_{S}
\end{aligned}
$$

-   **Tip**: $(u.v)^n \neq u^n.v^n$
-   **Tip**: $u^0 = \lambda$
-   **Tip**: $|u^k| = k.|u|$

---

### Reversal

Is **Reordering** chracters sequence of a string from **End-to-Begin**

$$
\begin{aligned}
    & S = abc
    \\
    & S^R = cba
\end{aligned}
$$

-   **Tip**: $u \neq u^R$
-   **Tip**: $(u.v)^R = v^R.u^R$
-   **Tip**: $(u^R)^n = (u^n)^R$
-   **Tip**: $(u^R)^R = u$
-   **Tip**: $|u| = |u^R|$

---

#### Palindrome

**Strings** that `equals` to their **Reverse**

$$
\begin{aligned}
    & u = aba
    \\ \implies
    & u = u^R
\end{aligned}
$$

-   **Tip**: Number of **Possible Palindromes** of size $n$ using an alphabet set $\Sigma$ is $|\Sigma|^{\lceil \frac{n}{2} \rceil}$
-   **Tip**: $u: Palindrome \implies u^n: Palindrome$
-   **Tip**: $u: Palindrome \implies u.u: Palindrome$
-   **Tip**: $u: Palindrome \iff u^R: Palindrome$

---
