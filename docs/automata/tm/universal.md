# Universal Turing Machine (UTM)

**Universal Turing Machine** is a **Turing Machine** that **Simulates** an `arbitrary turing machine` on `arbitrary input`

$$
\begin{aligned}
    & A_{TM} = \{<M,w> : \texttt{M is a TM and accepts w}\}
\end{aligned}
$$

The universal machine essentially achieves this by **Reading** both the **Description of the Machine** to be `simulated` as well as the **Input** to that machine `from` its own `tape`

![UTM](../../assets/utm.png)

---

## History

Alan turing introduced the concept of **Universal Turing Machine** that was the idea of **Stored-Program Computers**

Now we write our turing machine code in **C** or **C++** or **Java** or etc programming languages then, a **Universal Turing Machine** will **Simulates** it

---

## Coding

The **Coding Standard** for defining the `program` of a turing machine is:

$$
\begin{aligned}
    & <M> = 1^n \;00\; 1^m \;00\; Code_1 \;00\; Code_2 \;00\; \dots \;00\; Code_t \;00
    \\
    \\
    & 00: \text{Spliter}
    \\
    & n: \text{Number of states}
    \\
    & m: \text{Size of alphabet}
    \\
    & L = 1 \;\; R = 11
    \\
    & Code: \delta(q_i, x_j) = (q_k, x_l, R) \implies 1^i \;0\; 1^j \;0\; 1^k \;0\; 1^l \;0\; 11
\end{aligned}
$$

---

**Example**: What is the **UTM Code** of this **Turing Machine**:

![Coding Example](../../assets/utm_coding_example.png)

$$
\begin{aligned}
    & <M> = 1^3 \;00\; 1^3 \;00\; Codes
    \\
    & Code_1: \delta(q_1, a) = (q_1, b, R) \implies 1^1 0 1^1 0 1^1 0 1^2 0 1^2
    \\
    & Code_2: \delta(q_1, b) = (q_2, b, R) \implies 1^1 0 1^2 0 1^2 0 1^2 0 1^2
    \\
    & Code_3: \delta(q_2, b) = (q_1, a, R) \implies 1^2 0 1^2 0 1^1 0 1^1 0 1^2
    \\
    & Code_4: \delta(q_1, \Box) = (q_3, \Box, L) \implies 1^2 0 1^3 0 1^3 0 1^3 0 1^1
\end{aligned}
$$

---

## Countable

-   We can write a **UTM Code** for each **Turing Machine**
-   Each **UTM Code** is a **Natural Number**
-   Each **Language** over $\Sigma$ is **Countable**
-   Set of **All Subsets of Languages** over $\Sigma$ is **Uncountable**

$$
\begin{aligned}
    & \text{Set of all Turing Machines}:
    \begin{cases}
        \text{Infinite}
        \\
        \text{Countable}
    \end{cases}
    \\
    & \text{Set of all Languages}
    \begin{cases}
        \text{Infinite}
        \\
        \text{Uncountable}
    \end{cases}
\end{aligned}
$$

So there are **Infinite** number of **Languages** that doesn't have any **Turing Machine** called **Informal Languages**

---
