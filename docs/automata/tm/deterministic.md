# Deterministic Turing Machine (DTM)

**Deterministic Turing Machine** is an **Automata** consist of **Deterministic FSM** with **Array Memory** with **Infinite Size**, indicates with:

$$
\begin{aligned}
    & M = (Q, \Sigma, \Gamma, \delta, q_0, \Box, F)
    \\
    & \delta: Q \times \Gamma \mapsto Q \times \Gamma \times \{L,R\}
    \\
    & q_0 \in Q
    \\
    & F \subseteq Q
    \\
    & \Box \in \Gamma
    \\
    & \Sigma \subseteq \Gamma - \{\Box\}
    \\
    \\
    & Q: \text{States set}
    \\
    & \Sigma: \text{Input alphabet}
    \\
    & \Gamma: \text{Memory alphabet}
    \\
    & \delta: \text{Transition function}
    \\
    & q_0: \text{Initial state}
    \\
    & \Box: \text{Blank symbol}
    \\
    & F: \text{Final states}
\end{aligned}
$$

-   **Tip**: In **Turing Machines** the `input string` exists in **Read/Write Memory**($\Gamma$)
-   **Tip**: The **Blank Symbol**($\Box$) should not exist in **Input Alphabet**($\Sigma$) to detect the **End of Input**
-   $$
    \begin{aligned}
        & \Sigma \subseteq \Gamma - \{\Box\}
    \end{aligned}
    $$

---

## Language

**Language of a DTM** is all the strings that the **DTM** by **Accepting** them will **Halt** in **Final State**:

$$
\begin{aligned}
    & L(M) = \{w \in \Sigma^* : q_0 w \overset{*}{\vdash} x_1 q_f x_2 \;,\; x_1,x_2 \in \Gamma^* \;,\; q_f \in F\}
\end{aligned}
$$

---
