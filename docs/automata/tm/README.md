# Turing Machine (TM)

Is an **Automata** consists of **FSM** and **Array Memory** with **Infinite Size** for accepting **Recursively-Enumerable Languages**

![TM](../../assets/tm.png)

There are two categories of **Turing Machines**:

-   **Deterministic TM**: DTM
    -   The **FSM** `transitions` are always `deterministic`
    -   The **FSM** will `accept` when `halt` on `final state`
-   **Non-Deterministic TM**: NTM
    -   The **FSM** `transitions` can be `non-deterministic`
    -   The **FSM** will `accept` when `halt` at least on `one final state`

There are some tips:

-   **Tip**: Accepting a string doesn't mean in **TM**, it will **Transduce** it
-   **Tip**: The **Input String** exists in **Memory** and we can **Read/Write** over memory

---

## Transition

There are three ways to show **Transition Graph**:

![Transition](../../assets/tm_transition.png)

$$
\begin{aligned}
    & \delta(q, a) = (p, b, R)
    \\
    \\
    & q: \text{Current State}
    \\
    & a: \text{Current Memory}
    \\
    & p: \text{Next State}
    \\
    & b: \text{Next Memory}
    \\
    & R: \text{Move Left/Right}
\end{aligned}
$$

---

## Conversion

-   We can convert any **NTM** to an equivalent **DTM**
-   Every **DTM** is a type of **NTM**

![Conversion](../../assets/tm_conversion.png)

-   **Tip**: The power of **Turing Machines** is in the following order, which each one can describe a subset of **Recursively-Enumerable Languages**:
    -   $$
        \begin{aligned}
            & \underbrace{\text{TM} = \text{NTM} = \text{DTM}}_{\text{REL}} =
            \left.\begin{cases}
                \text{Queue PDA}
                \\
                \text{Two-Stack PDA}
                \\
                \text{Semi-Finite TM}
                \\
                \text{Stay-Option TM}
                \\
                \text{Multi-Slot TM}
                \\
                \text{Multi-Head TM}
                \\
                \text{Multi-Tape TM}
                \\
                \text{Multi-Dimension TM}
                \\
                \text{One-Way Two-Tape TM}
                \\
                \text{Two-Alphabet TM}
                \\
                \text{Three-State TM}
            \end{cases}\right\}
        \end{aligned}
        $$
    -   **Queue PDA**: Push-Down automata with **Queue**
    -   **Two-Stack PDA**: Push-Down automata with **Two Stacks**
    -   **Semi-Finite TM**: Turing machine with **Semi-Finite Tape**
    -   **Stay-Option TM**: Turing machine with **Left, Right, Stay** movements
        -   $$
            \begin{aligned}
                & \delta: Q \times \Gamma \mapsto Q \times \Gamma \times \{L,R,S\}
            \end{aligned}
            $$
    -   **Multi-Slot TM**: Turing machine with **One Head** and **Multiple Tapes**
        -   $$
            \begin{aligned}
                & \delta: Q \times \Gamma^n \mapsto Q \times \Gamma^n \times \{L,R\}
            \end{aligned}
            $$
    -   **Multi-Head TM**: Turing machine with **Multiple Heads** and **One Tape**
        -   $$
            \begin{aligned}
                & \delta: Q \times \Gamma^n \mapsto Q \times \Gamma^n \times \{L,R\}
            \end{aligned}
            $$
    -   **Multi-Tape TM**: Turing machine with **Multiple Heads** and **Multiple Tapes**
        -   $$
            \begin{aligned}
                & \delta: Q \times \Gamma^n \mapsto Q \times \Gamma^n \times \{L,R\}^n
            \end{aligned}
            $$
    -   **Multi-Dimension TM**: Turing machine with **One Head** and **Multi-Dimensional Tape**
        -   $$
            \begin{aligned}
                & \delta: Q \times \Gamma \mapsto Q \times \Gamma \times \{L,R,U,D\}
            \end{aligned}
            $$
    -   **One-Way Two-Tape TM**: Turing machine supports **One Way Movement** with **Two Tape**
        -   $$
            \begin{aligned}
                & \delta: Q \times \Gamma^2 \mapsto Q \times \Gamma^2 \times \{R\}^2
            \end{aligned}
            $$
    -   **Two-Alphabet TM**: Turing machine with only **Two Alphabet Symbols**
        -   $$
            \begin{aligned}
                & \delta: Q \times \{0,1\} \mapsto Q \times \{0,1\} \times \{L,R\}
            \end{aligned}
            $$
    -   **Three-State TM**: Turing machine with only **Three States**
        -   $$
            \begin{aligned}
                & \delta: \{q_0, q_1, q_2\} \times \Gamma \mapsto \{q_0, q_1, q_2\} \times \Gamma \times \{L,R\}
            \end{aligned}
            $$
-   **Tip**: Order types of turing machines are **Equal** in the **Computational Power**, but they may be better in **Time Complexity** and **Space Complexity**

---

## Configuration

Each pair of **Current FSM State** and **Current Position** and **Current Memory State** that can act by `movement` using `transition rules` of automata called a **Configuration**:

$$
\begin{aligned}
    & (XqY)
    \\
    & q: \text{FSM State}
    \\
    & X,Y: \text{Memory State}
    \\
    \\
    \text{Example}:
    \\
    & q_0 aabb \vdash cq_0abb \vdash ccq_0bb \vdash ccdq_1b \vdash ccddq_1
\end{aligned}
$$

---

## Turing Computable

**Turing Computable** or **Computable Functions** are functions that can be calculated using a **Turing Machine**

$$
\begin{aligned}
    & f: D \mapsto B
    \\
    & f = \{w \in D : q_0 w \overset{*}{\vdash} q_f f(w) \;,\; q_f \in F\}
\end{aligned}
$$

---

## Turing Complete

Automatons that can emulate the **Turing Machine** completely

-   **Turing Complete**: Programming language like **C**, **C++**, **Java**, **JavaScript**
-   **Turing UnComplete**: Data languages like **XML**, **JSON**, **HTML**, **CSS**

---

## Turing Thesis

A **Function** on the `natural numbers` can be calculated by an **Effective Method** if and only if it is `computable` by a **Turing Machine**

---

## TM => Language

Converting a **TM** to **Language** is very simple:

---

**Example 1**: Find **Unrestricted Expression** for this **TM**:

![Language Example 1](../../assets/tm_to_language_example_1.png)

$$
\begin{aligned}
    & L = \{a,b\}^* \{aa\} \{a,b\}^*
\end{aligned}
$$

---

## TM => Grammar

Converting a **TM** to **Grammar** is very simple:

-   **Convert** `TM` to `Language`
-   **Convert** `REL` to `Grammar`

---
