# Linear-Bounded Automata (LBA)

Is an **Automata** consists of **FSM** and **Array Memory** of **Size $O(n)$** `depends on input size` for accepting **Context-Sensitive Languages**

![LBA](../../assets/lba.jpg)

There are two categories of **Linear-Bounded Automatons**:

-   **Deterministic LBA**: DLBA
    -   The **FSM** `transitions` are always `deterministic`
    -   The **FSM** will `accept` when `halt` on `final state`
-   **Non-Deterministic LBA**: NLBA
    -   The **FSM** `transitions` can be `non-deterministic`
    -   The **FSM** will `accept` when `halt` at least on `one final state`

There are some tips:

-   **Tip**: The meaning of **LBA** is **NLBA**
-   **Tip**: Accepting a string doesn't mean in **LBA**, it will **Transduce** it
-   **Tip**: The **Input String** exists in **Memory** and we can **Read/Write** over memory
-   **Tip**: **Linear-Bounded Automatons** are a **Turing Machine** with **Memory of Size $O(n)$**

---

## Transition

There are three ways to show **Transition Graph**:

![Transition](../../assets/lba_transition.png)

$$
\begin{aligned}
    & \delta(q, a) = (p, b, R)
    \\
    \\
    & q: \text{Current State}
    \\
    & a: \text{Current Memory}
    \\
    & p: \text{Next State}
    \\
    & b: \text{Next Memory}
    \\
    & R: \text{Move Left/Right}
\end{aligned}
$$

---

## Conversion

-   We **Don't Know** if we can convert any **NLBA** to an equivalent **DLBA**
-   Every **DLBA** is a type of **NLBA**

![Conversion](../../assets/lba_conversion.png)

-   **Tip**: The power of **Linear-Bounded Automatons** is in the following order, which each one can describe a subset of **Context-Sensitive Languages**:
    -   $$
        \begin{aligned}
            & \underbrace{\text{LBA} = \text{NLBA}}_{\text{CSL}} \overset{?}{=} \underbrace{\text{DLBA}}_{\text{DCSL}}
        \end{aligned}
        $$

---

## Configuration

Each pair of **Current FSM State** and **Current Position** and **Current Memory State** that can act by `movement` using `transition rules` of automata called a **Configuration**:

$$
\begin{aligned}
    & (XqY)
    \\
    & q: \text{FSM State}
    \\
    & X,Y: \text{Memory State}
    \\
    \\
    \text{Example}:
    \\
    & q_0 aabb \vdash cq_0abb \vdash ccq_0bb \vdash ccdq_1b \vdash ccddq_1
\end{aligned}
$$

---

## LBA => Language

Converting a **LBA** to **Language** is very simple:

---

**Example 1**: Find **Unrestricted Expression** for this **LBA**:

![Language Example 1](../../assets/lba_to_language_example_1.png)

$$
\begin{aligned}
    & L = \{a,b\}^* \{aa\} \{a,b\}^*
\end{aligned}
$$

---

## LBA => Grammar

Converting a **LBA** to **Grammar** is very simple:

-   **Convert** `LBA` to `Language`
-   **Convert** `CSL` to `Grammar`

---
