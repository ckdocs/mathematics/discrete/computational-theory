# Deterministic Finite Automata (DFA)

**Deterministic Finite Automata** is an **Automata** consist of **Deterministic FSM** with **No Memory**, indicates with:

$$
\begin{aligned}
    & M = (Q, \Sigma, \delta, q_0, F)
    \\
    & \delta: Q \times \Sigma \mapsto Q
    \\
    & q_0 \in Q
    \\
    & F \subseteq Q
    \\
    \\
    & Q: \text{States set}
    \\
    & \Sigma: \text{Input alphabet}
    \\
    & \delta: \text{Transition function}
    \\
    & q_0: \text{Initial state}
    \\
    & F: \text{Final states}
\end{aligned}
$$

-   **Tip**: In **DFA** machines, $\delta$ function is **Complete** and **Deterministic**:
    -   **Complete**: For each state, it can input all **Alphabets**
    -   **Deterministic**: For each state, for each alphabet, it return **One Output**
-   **Tip**: After accepting a string by **DFA**, if it **Halts** on **Final State**, accepted
-   **Tip**: Each **DFA** contains only **One Initial State**
-   **Tip**: We **Cannot Reduce** the Minimized DFA **Final States**

---

## Language

**Language of a DFA** is all the strings that the **DFA** by **Accepting** them will **Halt** in **Final State**:

$$
\begin{aligned}
    & L(M) = \{w \in \Sigma^* : (q_0, w) \overset{*}{\vdash} (q_f, \lambda) \;,\; q_f \in F\}
    \\
    & L(M) = \{w \in \Sigma^* : \hat{\delta}(q_0, w) \in F\}
\end{aligned}
$$

---

## DFA Minimization

Before introducing the algorithms, we must **Remove** some states:

-   **Inaccessible State**: states which can never be `reached` (**No Input Edge**)

There are two ways of minimizing states of a **DFA**:

-   **Equivalence Theorem**: Using **GTG**
-   **Myphill-Nerode Theorem**: Using **Equations**

---

### Equivalence Theorem

Before the start we must introduce a concept:

-   **Distinguishable**: Two **States** $q_1$ and $q_2$ are **Distinguishable** in **Partition** $P_k$ for any **Input Symbol** $a$
    -   **If** $\delta(q_1, a)$ and $\delta(q_2, a)$ are in **Different Sets** in partition $P_{k-1}$.

Now we introduce the main algorithm:

-   **Create** two `disjoint sets` in parition $P_0$ contains:
    -   **Set 1**: All `non-final states`
    -   **Set 2**: All `final states`
-   **Iterate** for `number of states` (`until cannot split any set`):
    -   **Iterate** over all `states of sets`:
        -   **If** two state are `distinguishable`
            -   **MakeSet** the `state`
            -   **Union** the `state` with `non-distinguishable set`
            -   **If** cannot find `non-distinguishable set` create new `set`

---

**Example 1**: Minimize this **DFA**:

![Equivalence Example 1-1](../../assets/fa_dfa_minimization_equivalence_example_1_1.png)

$$
\begin{aligned}
    & P_0 = \{q_0, q_1, q_2, q_3\}, \{q_4\}
    \\
    & P_1 = \{q_0, q_1, q_2\}, \{q_3\}, \{q_4\}
    \\
    & P_2 = \{q_0, q_2\}, \{q_1\}, \{q_3\}, \{q_4\}
    \\
    & P_3 = \{q_0, q_2\}, \{q_1\}, \{q_3\}, \{q_4\}
\end{aligned}
$$

![Equivalence Example 1-2](../../assets/fa_dfa_minimization_equivalence_example_1_2.png)

---

### Myphill-Nerode Theorem

-   **Draw** a `table` for each `state pair`
-   **Mark** cells `(qi, qj)` with one `final` and one `non final` state
    -   $q_1 \in F \;AND\; q_2 \not\in F$
    -   $q_1 \not\in F \;AND\; q_2 \in F$
-   **Iterate** for `number of states` (`until cannot mark any cell`):
    -   **Iterate** over `unmarked cells`:
        -   **If** $(\delta(q_i, A), \delta(q_j, A))$ is `marked` for some `A` input
            -   **Mark** the `cell`
-   **Combine** all `unmarked` pairs `states`

---

**Example 1**: Minimize this **DFA**:

![Myphill-Nerode Example 1-1](../../assets/fa_dfa_minimization_myphill_nerode_example_1_1.png)

$$
\begin{aligned}
    & \left.\begin{cases}
        \delta(f, 0) = f
        \\
        \delta(a, 0) = b
    \end{cases}\right\} \implies (f,b) \times
    \\
    & \left.\begin{cases}
        \delta(f, 1) = f
        \\
        \delta(a, 1) = d
    \end{cases}\right\} \implies (f,d) \checkmark
    \\
    \\
    & \left.\begin{cases}
        \delta(f, 0) = f
        \\
        \delta(b, 0) = a
    \end{cases}\right\} \implies (f,a) \times
    \\
    & \left.\begin{cases}
        \delta(f, 1) = f
        \\
        \delta(b, 1) = c
    \end{cases}\right\} \implies (f,c) \checkmark
\end{aligned}
$$

![Myphill-Nerode Example 1-2](../../assets/fa_dfa_minimization_myphill_nerode_example_1_2.png)

---
