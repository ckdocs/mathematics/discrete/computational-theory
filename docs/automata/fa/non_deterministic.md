# Non-Deterministic Finite Automata (DFA)

**Non-Deterministic Finite Automata** is an **Automata** consist of **Non-Deterministic FSM** with **No Memory**, indicates with:

$$
\begin{aligned}
    & M = (Q, \Sigma, \delta, q_0, F)
    \\
    & \delta: Q \times (\Sigma \cup \{\lambda\}) \mapsto 2^Q
    \\
    & q_0 \in Q
    \\
    & F \subseteq Q
    \\
    \\
    & Q: \text{States set}
    \\
    & \Sigma: \text{Input alphabet}
    \\
    & \delta: \text{Transition function}
    \\
    & q_0: \text{Initial state}
    \\
    & F: \text{Final states}
\end{aligned}
$$

![NFA](../../assets/fa_nfa.gif)

-   **Tip**: In **NFA** machines, $\delta$ function is **Non-Complete** and **Non-Deterministic**:
    -   **Non-Complete**: There are some states, cannot accept all **Alphabets** (Machine will **Halt**)
    -   **Non-Deterministic**: For each state, for each alphabet, it return **Many Output**
-   **Tip**: After accepting a string by **NFA**, if it **Halts** on **At-least One Final State**, accepted
-   **Tip**: We **Can Reduce** the NFA **Initial State** to **One**
    -   **Create** new initial state
    -   **Connect** it with $\lambda$ to old initial states
-   **Tip**: We **Can Reduce** the NFA **Final State** to **One**
    -   **Create** new final state
    -   **Connect** old final states with $\lambda$ to it

---

## Language

**Language of a NFA** is all the strings that the **NFA** by **Accepting** them will **Halt** in **At Least One Final State**:

$$
\begin{aligned}
    & L(M) = \{w \in \Sigma^* : (q_0, w) \overset{*}{\vdash} (q_f, \lambda) \;,\; q_f \in F\}
    \\
    & L(M) = \{w \in \Sigma^* : \hat{\delta}(q_0, w) \cap F \neq \emptyset\}
\end{aligned}
$$

---

## ε-Closure

**Epsilon Closure** for a given state **X** is a set of states which **Can be Reached** from the states X with only **(null)** or **ε** moves including the state X **Itself**.

---

**Example**: Find the **Epsilon Closures** of all states:

![Epsilon Closure](../../assets/fa_epsilon_closure.png)

$$
\begin{aligned}
    & \epsilon(A) = \{A, \epsilon(B)\}
    \\
    & \epsilon(B) = \{B, \epsilon(C)\}
    \\
    & \epsilon(C) = \{C\}
    \\
    \\ \implies
    & \epsilon(A) = \{A, B, C\}
    \\
    & \epsilon(B) = \{B, C\}
    \\
    & \epsilon(C) = \{C\}
\end{aligned}
$$

---

## NFA => DFA

The power of **NFA** and **DFA** is equal, and we can convert any **NFA** to an equivalent **DFA**:

-   **Find** all `epsilon closures` of **NFA** states
-   **Create** `state table` for **NFA** contains `epsilon closure`
-   **Mark** `initial state` of **NFA** as `initial state` of **DFA**
-   **Grow** the `states` of **DFA** by:
    -   **Foreach** state and $\epsilon.\Sigma.\epsilon$ find the `target states`
    -   **Store** the `state set` if it was new
-   **Each** `state set` that contains **NFA** `final state` is final
-   **If** the **NFA** can accept $\lambda$ `initial state` is `final`

---

**Example 1**: Find the **Equivalent DFA** of this **NFA**:

![NFA to DFA Example 1](../../assets/fa_nfa_to_dfa_example_1.png)

| **State** \ **Input** | $\epsilon$  | $0$       | $1$     |
| --------------------- | ----------- | --------- | ------- |
| $A$                   | $\{A,B,C\}$ | $\{B,C\}$ | $\{A\}$ |
| $B$                   | $\{B,C\}$   | $\{\}$    | $\{B\}$ |
| $C$                   | $\{C\}$     | $\{C\}$   | $\{C\}$ |

$$
\begin{aligned}
    & \{A\} \overset{\epsilon}{\implies} \{A,B,C\} \overset{0}{\implies} \{B,C\} \overset{\epsilon}{\implies} \{B,C\}
    \\
    & \{A\} \overset{\epsilon}{\implies} \{A,B,C\} \overset{1}{\implies} \{A,B,C\} \overset{\epsilon}{\implies} \{A,B,C\}
    \\
    \\
    & \{B,C\} \overset{\epsilon}{\implies} \{B,C\} \overset{0}{\implies} \{C\} \overset{\epsilon}{\implies} \{C\}
    \\
    & \{B,C\} \overset{\epsilon}{\implies} \{B,C\} \overset{1}{\implies} \{B,C\} \overset{\epsilon}{\implies} \{B,C\}
    \\
    \\
    & \{A,B,C\} \overset{\epsilon}{\implies} \{A,B,C\} \overset{0}{\implies} \{B,C\} \overset{\epsilon}{\implies} \{B,C\}
    \\
    & \{A,B,C\} \overset{\epsilon}{\implies} \{A,B,C\} \overset{1}{\implies} \{A,B,C\} \overset{\epsilon}{\implies} \{A,B,C\}
    \\
    \\
    & \{C\} \overset{\epsilon}{\implies} \{C\} \overset{0}{\implies} \{C\} \overset{\epsilon}{\implies} \{C\}
    \\
    & \{C\} \overset{\epsilon}{\implies} \{C\} \overset{1}{\implies} \{C\} \overset{\epsilon}{\implies} \{C\}
\end{aligned}
$$

| **State** \ **Input** | $\epsilon.0.\epsilon$ | $\epsilon.1.\epsilon$ |
| --------------------- | --------------------- | --------------------- |
| $\{A\}$               | $\{B,C\}$             | $\{A,B,C\}$           |
| $\{B,C\}$             | $\{C\}$               | $\{B,C\}$             |
| $\{A,B,C\}$           | $\{B,C\}$             | $\{A,B,C\}$           |
| $\{C\}$               | $\{C\}$               | $\{C\}$               |

![NFA to DFA Answer 1](../../assets/fa_nfa_to_dfa_answer_1.png)

---
