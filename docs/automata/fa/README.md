# Finite Automata (FA)

Is an **Automata** consists of **FSM** and **No-Memory** for accepting **Regular Languages**

![FA](../../assets/fa.png)

There are two categories of **Finite Automatons**:

-   **Deterministic FA**: DFA
    -   The **FSM** `transitions` are always `deterministic`
    -   The **FSM** will `accept` when `halt` on `final state`
-   **Non-Deterministic FA**: NFA
    -   The **FSM** `transitions` can be `non-deterministic`
    -   The **FSM** will `accept` when `halt` at least on `one final state`

---

## Transition

There are one way to show **Transition Graph**:

![Transition](../../assets/fa_transition.png)

$$
\begin{aligned}
    & \delta(q, a) = p
    \\
    \\
    & q: \text{Current State}
    \\
    & a: \text{Input symbol}
    \\
    & p: \text{Next State}
\end{aligned}
$$

---

## Conversion

-   We can convert any **NFA** to an equivalent **DFA**
-   Every **DFA** is a type of **NFA**

![Conversion](../../assets/fa_conversion.png)

-   **Tip**: The power of **Finite Automatons** is in the following order, which each one can describe a subset of **Regular Languages**:
    -   $$
        \begin{aligned}
            & \underbrace{\text{NFA} = \text{DFA}}_{\text{RL}} =
            \left.\begin{cases}
                \text{Finite-Stack PDA}
                \\
                \text{Read-Only TM}
                \\
                \text{One-Way TM}
            \end{cases}\right\}
        \end{aligned}
        $$
    -   **Finite-Stack PDA**: Push-Down automata with **Finite Stack Size**
    -   **Read-Only TM**: Turing machine **Without Write**
    -   **One-Way TM**: Turing machine **Cannot Move Back**

---

## Configuration

Each pair of **Current FSM State** and **Remaining Input** that can act by `movement` using `transition rules` of automata called a **Configuration**:

$$
\begin{aligned}
    & (q, I)
    \\
    & q: \text{FSM State}
    \\
    & I: \text{Remaining Input}
    \\
    \\
    \text{Example}:
    \\
    & (q_0, aabb) \vdash (q_0, abb) \vdash (q_1, bb) \vdash (q_1, b) \vdash (q_f, \lambda)
\end{aligned}
$$

---

## FA => Language

There are two ways of finding a **Regular Expression** of a **Finite Automata**:

-   **State Elimination Method**: Using **GTG**
-   **Arden's Method**: Using **Equations**

---

### State Elimination Method

Is a method for finding **Regular Expression** for any **DFA**

-   **Create** new `initial state` without incoming edges (**One Initial**)
-   **Create** new `final state` without outcoming edges (**One Final**)
-   **Remove** all `intermediate states` one by one and find **GTG**

![State Elimination Method](../../assets/fa_to_language_elimination_method.png)

---

**Example 1**: Find **Regular Expression** for this **DFA**:

![State Elimination Method Example 1](../../assets/fa_to_language_elimination_method_example_1.png)

---

**Example 2**: Find **Regular Expression** for this **DFA**:

![State Elimination Method Example 2](../../assets/fa_to_language_elimination_method_example_2.png)

---

**Example 3**: Find **Regular Expression** for this **DFA**:

![State Elimination Method Example 3](../../assets/fa_to_language_elimination_method_example_3.png)

---

**Example 4**: Find **Regular Expression** for this **DFA**:

![State Elimination Method Example 4](../../assets/fa_to_language_elimination_method_example_4.png)

---

### Arden's Method

Is a method for finding **Regular Expression** for any **DFA** and **NFA**

<!-- TODO -->

---

## FA => Grammar

Converting a **NFA** or **DFA** to **Grammar** is very simple:

-   **Convert** each state to a **Grammar Variable**
-   **Convert** each edge to a **Right Linear Rule**
-   **Add** lambda rule for each **Final State**

---

**Example 1**: Find **Regular Grammar** for this **DFA**:

![Grammar Example 1](../../assets/fa_to_grammar_example_1.png)

$$
\begin{aligned}
    & A \mapsto 0B
    \\
    & B \mapsto 1A \;|\; \lambda
\end{aligned}
$$

---

**Example 2**: Find **Regular Grammar** for this **DFA**:

![Grammar Example 2](../../assets/fa_to_grammar_example_2.png)

$$
\begin{aligned}
    & q_1 \mapsto a q_2
    \\
    & q_2 \mapsto b q_4 \;|\; c q_3 \;|\; d q_5
    \\
    & q_3 \mapsto \lambda
    \\
    & q_4 \mapsto \lambda
    \\
    & q_5 \mapsto \lambda
\end{aligned}
$$

---

**Example 3**: Find **Regular Grammar** for this **DFA**:

![Grammar Example 3](../../assets/fa_to_grammar_example_3.png)

$$
\begin{aligned}
    & q_1 \mapsto c q_1 \;|\; a q_2
    \\
    & q_2 \mapsto b q_1 \;|\; d q_2 \;|\; \lambda
\end{aligned}
$$

---

**Example 4**: Find **Regular Grammar** for this **DFA**:

![Grammar Example 4](../../assets/fa_to_grammar_example_4.png)

$$
\begin{aligned}
    & A \mapsto aB \;|\; bC \;|\; \lambda
    \\
    & B \mapsto bA
    \\
    & C \mapsto bB \;|\; aA
\end{aligned}
$$

---
