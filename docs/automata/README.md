# Automata

Is a machine that **Accept** a `formal language` and **Do Something**, indicates with $M$ consists of two parts:

-   **Control Unit**:
    -   Is a **FSM (Finite State Machine)** that accept a language and do something
-   **Memory**:
    -   Is a **Temporary Space** that can read, write by control unit

---

## Concepts

There are many important concepts in **Automata**:

-   **Transition**: Automata `rules` for movement
-   **Configuration**: Each state of `input`, `memory`, `fsm`
-   **Movement**: Movement from `one configuration` to `another` by `transition rules`
-   **Determinism**: Number of `possible` movements from a `configuration`
-   **Language**: All strings that can `accept` by automata `movements`

---

### FSM

**Finite State Machine** is a machine consists of **Finite** number of **States**, it start from **Begin State** and for each **Input** may change it's state

![FSM](../assets/automata_fsm.png)

There are many models of **Finite State Machines** based on their **Input,Output**:

-   **Acceptor**: Produce **Binary** output (`accept`, `reject`)
-   **Classifier**: Produce **Enumerate** output (`1`, `2`, ...)
-   **Transducer**: Produce **String** output (`13dasa`, ...)
-   **Sequencer**: Produce **String** output (`13dasa`, ...)

---

#### Acceptor

Is a **FSM** which accepts an input and return **Binary** output:

-   **Accept**: Halt on **Final States**
-   **Reject**: Halt on **Non-Final States**

![FSM Acceptor](../assets/automata_fsm_acceptor.png)

---

#### Classifier

Is a **FSM** which accepts an input and return **Enumerate** output:

-   **Class 1**: Halt on **Class-1 States**
-   **Class 1**: Halt on **Class-2 States**
-   ...
-   **Class k**: Halt on **Class-k States**

![FSM Classifier](../assets/automata_fsm_classifier.png)

---

#### Transducer

Is a **FSM** which accepts an input and print **Character** on each **Configuration**

There are two types of **Transducer** machines:

-   **Mealy Machine**: Print characters on **Transitions**
-   **Moore Machine**: Print characters on **States**

![FSM Transducer](../assets/automata_fsm_transducer.png)

---

#### Sequencer

Is a **FSM** which accepts only **Signals (one character)** input and print **Character** on each **Configuration**

Also called **Generator** machines

![FSM Sequencer](../assets/automata_fsm_sequencer.png)

---

### Transition

All automata **Rules** used for **Movement** between `configurations`

There are three ways of describing **Transition Rules** of an **Automata**:

-   **Transition Graph**: Is a visualization of **FSM** and it's **States**
-   **Transition Table**: Is a table, contains **FSM** transitions rules for each **Input,Memory,State**
-   **Transition Function**: Is function, that inputs **Input,Memory,State** and outputs **Memory,State**

![Transition](../assets/automata_transition.png)

---

#### Generalized Transition Graph

Is a type of **Transition Graph** whose edges are labeled with **Language Expressions**

![Transition GTG](../assets/automata_transition_gtg.png)

---

#### Extended Transition Function

The **Extended Transition Function** tells us the **Final States** of **FSM** after accepting the **Entire Input String**:

$$
\begin{aligned}
    & \hat{\delta}: Q \times \Sigma^* \mapsto \{Q\}
    \\
    & \hat{\delta}(q, au) = \hat{\delta}(\delta(q, a), u)
    \\
    & \hat{\delta}(q, uv) = \hat{\delta}(\hat{\delta}(q, u), v)
    \\
    \\
    \text{Example}:
    \\
    & \hat{\delta}(S_1, ac) = \{S_3, S_4\}
\end{aligned}
$$

-   **Tip**: In **DFA**, transition function output is **Single State**
-   **Tip**: In **NFA**, transition function output is **Multiple States**

![Transition ETF](../assets/automata_transition_etf.png)

---

### Configuration

Each pair of **Current FSM State** and **Remaining Input** and **Current Memory State** that can act by `movement` using `transition rules` of automata called a **Configuration**

$$
\begin{aligned}
    & (q, I, M)
    \\
    \\
    & q: \text{FSM State}
    \\
    & I: \text{Remaining Input}
    \\
    & M: \text{Memory State}
\end{aligned}
$$

---

**Example**:

$$
\begin{aligned}
    & M:
    \left.\begin{cases}
        \delta(q_0, a, Z) = (q_0, 0Z)
        \\
        \delta(q_0, a, 0) = (q_0, 00)
        \\
        \delta(q_0, \lambda, 0) = (p, 0)
        \\
        \delta(p, b, 0) = (p, \lambda)
        \\
        \delta(p, b, Z) = (p, \lambda)
    \end{cases}\right\}
    \\
    \\
    & \underbrace{(q_0, 01, Z) \vdash (q_0, 1, 0Z) \vdash (p, 1, 0Z) \vdash (p, \lambda, Z)}_{\text{Configuration}}
\end{aligned}
$$

---

### Movement

Moving from one **Configuration** to another by applying a **Transition Rule**, called a **Movement**

$$
\begin{aligned}
    &
    \left.\begin{cases}
        \text{State}
        \\
        \text{Input}
        \\
        \text{Memory}
    \end{cases}\right\}
    \underbrace{\implies}_{\text{Transition Rule}}
    \left.\begin{cases}
        \text{State}
        \\
        \text{Memory}
    \end{cases}\right\}
\end{aligned}
$$

There are two ways of describing **Movement Path** to accept a **String** from **Initial State**:

-   **Movement Tree**: Is a tree that shows the **Transition Rules** that used
-   **Movement Sequence**: Is a sequence of **Transition Rules** that used
    -   There are three **Movement Symbols**:
        -   $\overset{}{\vdash}$: **One step** movement
        -   $\overset{+}{\vdash}$: **1 to k steps** movement
        -   $\overset{*}{\vdash}$: **0 to k steps** movement

---

**Example**:

$$
\begin{aligned}
    & M:
    \left.\begin{cases}
        \delta(q_0, a, Z) = (q_0, 0Z)
        \\
        \delta(q_0, a, 0) = (q_0, 00)
        \\
        \delta(q_0, \lambda, 0) = (p, 0)
        \\
        \delta(p, b, 0) = (p, \lambda)
        \\
        \delta(p, b, Z) = (p, \lambda)
    \end{cases}\right\}
    \\
    \\
    & (q_0, 01, Z) \underbrace{\vdash}_{\text{Movement}} (q_0, 1, 0Z) \underbrace{\vdash}_{\text{Movement}} (p, 1, 0Z) \underbrace{\vdash}_{\text{Movement}} (p, \lambda, Z)
\end{aligned}
$$

---

### Computation

A **Sequence** of `movements` over an **Input String** by an `automata` to **Halt** the machine

-   **Halting**: Stopping the automata after **Finite** number of `movements`
-   **Looping**: Running the automata for **Infinite** number of `movements`

There are some tips about **Loops** and **Halts**:

-   **Tip**: We can find a **NFA** with **Infinite Loop**
-   **Tip**: We can find a **NPDA** with **Infinite Loop**
-   **Tip**: We can find a **DLBA** or **NLBA** with **Infinite Loop**
-   **Tip**: We can find a **DTM** or **NTM** with **Infinite Loop**

---

### Complexity

Number of `movements` to **Halt** by accepting a `string` of size **n** by an automata called **Complexity**

---

**Example**: Find the complexity of **Turing Machine** bellow:

![Complexity Example](../assets/automata_complexity_example.png)

$$
\begin{aligned}
    & L(M) = \{a^n b^n : n \geq 0\}
    \\
    \\
    & Complexity(M) = 2n \times n = 2n^2 \in O(n^2)
\end{aligned}
$$

---

### Determinism

There are two categories of **Automatons**:

-   **Deterministic**: In each **Configuration** automata has only **One Possible Transition**
    -   No **Same** symbol out's from a state
    -   If $\lambda$ out's from a state, there is **No Other Output**
-   **Non-Deterministic**: In each **Configuration** automata has **Many Possible Transitions**

![Determinism](../../assets/automata_determinism.png)

---

**Example**:

![Determinism Example](../assets/automata_determinism_example.png)

$$
\begin{aligned}
    & \text{Deterministic Automata}:
    \begin{cases}
        \delta(0, a) = \{1\}
        \\
        \delta(0, b) = \{0\}
        \\
        \delta(1, a) = \{1\}
        \\
        \delta(1, b) = \{2\}
        \\
        \delta(2, a) = \{1\}
        \\
        \delta(2, b) = \{0\}
    \end{cases}
    \\
    \\
    & \text{Non-Deterministic Automata}:
    \begin{cases}
        \delta(0, a) = \{0, 1\}
        \\
        \delta(0, b) = \{0\}
        \\
        \delta(1, a) = \{\}
        \\
        \delta(1, b) = \{2\}
        \\
        \delta(2, a) = \{\}
        \\
        \delta(2, b) = \{\}
    \end{cases}
\end{aligned}
$$

---

### Language

**Language of an automata** is all the strings that **Automata** can **Accept**:

$$
\begin{aligned}
    & L(M) = \{w \in \Sigma^* : \hat{\delta}(q_0, w) \cap F \neq \emptyset\}
    \\
    & \overline{L(M)} = \{w \in \Sigma^* : \hat{\delta}(q_0, w) \cap F = \emptyset\}
\end{aligned}
$$

-   **Tip**: There are **Infinite** number of **Automatons** for each **Language**
    -   If we can find at least **One Automata** for a language, we can find $\infty$ number of automatons
-   **Tip**: There is only **One Language** for each **Automata**
-   **Tip**: The **Minimized Automata** for each language is **Unique**

![Language](../assets/automata_language.png)

---

#### Equivalent

Two automatons are **Equivalent** if and only if their **Languages** are equal:

$$
\begin{aligned}
    \left.\begin{cases}
        L(M_1) \subseteq L(M_2)
        \\
        L(M_2) \subseteq L(M_1)
    \end{cases}\right\} \iff L(M_1) = L(M_2) \iff M_1 = M_2
\end{aligned}
$$

---

## Categories

The **Power** of an automata depends on it's **Memory**, there are four categories of automatons:

-   **Finite** (FA):
    -   Machine for accepting **Regular** languages
    -   **No Memory**
-   **Push-Down** (PDA):
    -   Machine for accepting **Context-free** languages
    -   **Stack Memory**
-   **Linear Bounded** (LBA):
    -   Machine for accepting **Context-sensitive** languages
    -   **Finite Array Memory**
-   **Turing Machine** (TM):
    -   Machine for accepting **Recursively enumerable** languages
    -   **Infinite Array Memory**

---
