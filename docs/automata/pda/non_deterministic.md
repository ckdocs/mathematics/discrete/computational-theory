# Non-Deterministic Push-Down Automata (DPDA)

**Deterministic Push-Down Automata** is an **Automata** consist of **Non-Deterministic FSM** with **Stack Memory** that accept strings by **Final-State** or **Empty-Stack**, indicates with:

$$
\begin{aligned}
    & M = (Q, \Sigma, \Gamma, \delta, q_0, Z, F)
    \\
    & \delta: Q \times (\Sigma \cup \{\lambda\}) \mapsto 2^{Q \times \Gamma^*}
    \\
    & q_0 \in Q
    \\
    & Z \in \Gamma
    \\
    & F \subseteq Q
    \\
    \\
    & Q: \text{States set}
    \\
    & \Sigma: \text{Input alphabet}
    \\
    & \Gamma: \text{Stack alphabet}
    \\
    & \delta: \text{Transition function}
    \\
    & q_0: \text{Initial state}
    \\
    & Z: \text{Initial stack}
    \\
    & F: \text{Final states}
\end{aligned}
$$

---

## Language

**Language of a NPDA** is all the strings that the **NPDA** by **Accepting** them will **Halt** in **Final State** or with **Empty Stack**:

$$
\begin{aligned}
    & L(M) = \{w \in \Sigma^* : (q_0, w, Z) \overset{*}{\vdash} (q_f, \lambda, \gamma) \;,\; q_f \in F \;,\; \gamma \in \Gamma^*\}
    \\
    & L(M) = \{w \in \Sigma^* : (q_0, w, Z) \overset{*}{\vdash} (p, \lambda, \lambda) \;,\; p \in Q\}
\end{aligned}
$$

---
