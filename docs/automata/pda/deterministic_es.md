# Deterministic Push-Down Automata Empty-Stack (DPDA-ES)

**Deterministic Push-Down Automata Empty-Stack** is an **Automata** consist of **Deterministic FSM** with **Stack Memory** that accept strings when **Halt With Empty Stack**, indicates with:

$$
\begin{aligned}
    & M = (Q, \Sigma, \Gamma, \delta, q_0, Z, F)
    \\
    & \delta: Q \times (\Sigma \cup \{\lambda\}) \mapsto Q \times \Gamma^*
    \\
    & q_0 \in Q
    \\
    & Z \in \Gamma
    \\
    & F \subseteq Q
    \\
    \\
    & Q: \text{States set}
    \\
    & \Sigma: \text{Input alphabet}
    \\
    & \Gamma: \text{Stack alphabet}
    \\
    & \delta: \text{Transition function}
    \\
    & q_0: \text{Initial state}
    \\
    & Z: \text{Initial stack}
    \\
    & F: \text{Final states}
\end{aligned}
$$

---

## Language

**Language of a DPDA-ES** is all the strings that the **DPDA-ES** by **Accepting** them will **Halt** with **Empty Stack**:

$$
\begin{aligned}
    & L(M) = \{w \in \Sigma^* : (q_0, w, Z) \overset{*}{\vdash} (p, \lambda, \lambda) \;,\; p \in Q\}
\end{aligned}
$$

---
