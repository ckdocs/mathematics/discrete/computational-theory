# Push-Down Automata (PDA)

Is an **Automata** consists of **FSM** and **Stack Memory** for accepting **Context-Free Languages**

![PDA](../../assets/pda.png)

There are four categories of **Push-Down Automatons**:

-   **Deterministic PDA Final-State**: DPDA-FS
    -   The **FSM** `transitions` are always `deterministic`
    -   The **FSM** will `accept` when `halt` on `final state`
-   **Deterministic PDA Empty-Stack**: DPDA-ES
    -   The **FSM** `transitions` are always `deterministic`
    -   The **FSM** will `accept` when `halt` with `empty stack`
-   **Non-Deterministic PDA Final-State**: NPDA-FS
    -   The **FSM** `transitions` can be `non-deterministic`
    -   The **FSM** will `accept` when `halt` at least on `one final state`
-   **Non-Deterministic PDA Empty-Stack**: NPDA-ES
    -   The **FSM** `transitions` can be `non-deterministic`
    -   The **FSM** will `accept` when `halt` at least with `one empty stack`

There are some tips:

-   **Tip**: The meaning of **PDA** is **NPDA**

---

## Transition

There are three ways to show **Transition Graph**:

![Transition](../../assets/pda_transition.png)

$$
\begin{aligned}
    & \delta(q, a, Z) = (p, 0Z)
    \\
    \\
    & q: \text{Current State}
    \\
    & a: \text{Input symbol}
    \\
    & Z: \text{Current Memory}
    \\
    & p: \text{Next State}
    \\
    & 0Z: \text{Next Memory}
\end{aligned}
$$

---

## Conversion

-   We can convert any **NPDA-FS** to an equivalent **NPDA-ES**
-   We can convert any **NPDA-ES** to an equivalent **NPDA-FS**
-   We can convert any **DPDA-FS** to an equivalent **DPDA-ES**
-   Every **DPDA** is a type of **NPDA**

![Conversion](../../assets/pda_conversion.png)

-   **Tip**: The power of **Push-Down Automatons** is in the following order, which each one can describe a subset of **Context-Free Languages**:
    -   $$
        \begin{aligned}
            & \underbrace{\text{PDA} = \text{NPDA} = \text{NPDA-FS} = \text{NPDA-ES}}_{\text{CFL}} \geq \underbrace{\text{DPDA-FS}}_{\text{DCFL}} \geq \underbrace{\text{DPDA-ES}}_{PFL}
        \end{aligned}
        $$

---

## Configuration

Each pair of **Current FSM State** and **Remaining Input** and **Current Memory State** that can act by `movement` using `transition rules` of automata called a **Configuration**:

$$
\begin{aligned}
    & (q, I, M)
    \\
    & q: \text{FSM State}
    \\
    & I: \text{Remaining Input}
    \\
    & M: \text{Memory State}
    \\
    \\
    \text{Example}:
    \\
    & (q_0, aabb, Z) \vdash (q_0, abb, 0Z) \vdash (q_1, bb, 00Z) \vdash (q_1, b, 0Z) \vdash (q_f, \lambda, Z)
\end{aligned}
$$

---

## PDA => Language

Converting a **Push-Down Automata** to **Language** is very simple:

-   **Convert** `PDA` to `Grammar`
-   **Convert** `CFG` to `Language`

---

## PDA => Grammar

Converting a **PDA** to **Grammar** is very simple:

-   **Simplify** the `PDA`:
    -   PDA should `have only` **One Final State**
    -   PDA should **Empty Stack** before `accepting`
    -   PDA should `have only` **Pop or Push Transition** `not both`
    -   ![Automata to Grammar Simplification](../../assets/pda_to_grammar_simplification.png)
-   **Create** `variables` for each `pair of states`: $A_{q_0 q_0} A_{q_0 q_1} \dots A_{q_f q_f}$
-   **Starting** `variable` is $A_{q_0 q_f}$
-   **Create** `production rules`:
    -   ![Automata to Grammar Rules](../../assets/pda_to_grammar_rules.png)

---

**Example 1**: Find **Context Free Grammar** for this **PDA**:

![Grammar Example 1](../../assets/pda_to_grammar_example_1.jpg)

$$
\begin{aligned}
    & \text{Path productions}:
    \begin{cases}
        A_{01} \longrightarrow A_{01}A_{11}
        \\
        A_{02} \longrightarrow A_{01}A_{12} \;|\; A_{02}A_{22}
        \\
        A_{03} \longrightarrow A_{01}A_{13} \;|\; A_{02}A_{23}
        \\
        A_{11} \longrightarrow A_{11}A_{11}
        \\
        A_{12} \longrightarrow A_{11}A_{12} \;|\; A_{12}A_{22}
        \\
        A_{13} \longrightarrow A_{11}A_{13} \;|\; A_{12}A_{23}
        \\
        A_{22} \longrightarrow A_{22}A_{22}
        \\
        A_{23} \longrightarrow A_{22}A_{23}
    \end{cases}
    \\
    & \text{Null productions}:
    \begin{cases}
        A_{00} \longrightarrow \lambda
        \\
        A_{11} \longrightarrow \lambda
        \\
        A_{22} \longrightarrow \lambda
        \\
        A_{33} \longrightarrow \lambda
    \end{cases}
    \\
    & \text{Move productions}:
    \begin{cases}
        A_{12} \longrightarrow 0 A_{12} 1
        \\
        A_{12} \longrightarrow 0 A_{11}
        \\
        A_{03} \longrightarrow A_{12}
    \end{cases}
\end{aligned}
$$

---
