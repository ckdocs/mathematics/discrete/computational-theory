# Deterministic Push-Down Automata Final-State (DPDA-FS)

**Deterministic Push-Down Automata Final-State** is an **Automata** consist of **Deterministic FSM** with **Stack Memory** that accept strings when **Halt In Final State**, indicates with:

$$
\begin{aligned}
    & M = (Q, \Sigma, \Gamma, \delta, q_0, Z, F)
    \\
    & \delta: Q \times (\Sigma \cup \{\lambda\}) \mapsto Q \times \Gamma^*
    \\
    & q_0 \in Q
    \\
    & Z \in \Gamma
    \\
    & F \subseteq Q
    \\
    \\
    & Q: \text{States set}
    \\
    & \Sigma: \text{Input alphabet}
    \\
    & \Gamma: \text{Stack alphabet}
    \\
    & \delta: \text{Transition function}
    \\
    & q_0: \text{Initial state}
    \\
    & Z: \text{Initial stack}
    \\
    & F: \text{Final states}
\end{aligned}
$$

---

## Language

**Language of a DPDA-FS** is all the strings that the **DPDA-FS** by **Accepting** them will **Halt** in **Final State**:

$$
\begin{aligned}
    & L(M) = \{w \in \Sigma^* : (q_0, w, Z) \overset{*}{\vdash} (q_f, \lambda, \gamma) \;,\; q_f \in F \;,\; \gamma \in \Gamma^*\}
\end{aligned}
$$

---
